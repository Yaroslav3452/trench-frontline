﻿using System.Collections.Generic;
using System.Linq;
using Interfaces;
using UnityEngine;

public class Battle : MonoBehaviour
{
   public static Battle Current;
   public List<BattleInSector> BattlesList = new List<BattleInSector>();
   private float _time;

   [System.Serializable]
   public class BattleInSector
   {
      public List<GameObject> AttackSide;
      public List<GameObject> DefendSide;
      public SectorInfo AttackersSector;
      public SectorInfo DefendersSector;

      public BattleInSector(SectorInfo attackersSector, SectorInfo defendersSector, List<GameObject> attackUnits)
      {
         AttackSide = new List<GameObject>();
         AttackSide.AddRange(attackUnits);
         DefendSide = new List<GameObject>();
         DefendSide.AddRange(defendersSector.cellsUnits);
         AttackersSector = attackersSector;
         DefendersSector = defendersSector;
      }

      public void ClearBattle()
      {
         AttackSide.Clear();
         DefendSide.Clear();
         AttackersSector = null;
         DefendersSector = null;
      }
   }

   public void StartBattle(SectorInfo attackerSector, SectorInfo defenderSector, List<GameObject> attackUnits)
   {
      foreach (var battleStruct in BattlesList)
      {
         //если сектор уже защищается от другого
         if (battleStruct.DefendersSector == defenderSector)
         {
            foreach (var unit in attackUnits)
            {
               battleStruct.AttackSide.Add(unit);
               unit.GetComponent<IBattleSetter>().SetCurrentBattle(battleStruct);
               unit.GetComponent<IAttackSectorSetter>().SetAttackSector(defenderSector);
            }

            if (battleStruct.DefendSide.Count != 0) SetAttackTargets(battleStruct);
            return;
         }

         //если сектор уже атакует кого-то
         foreach (var defendUnit in defenderSector.cellsUnits.ToList())
         {
            if (defendUnit == null) continue;
            if (battleStruct.AttackSide.Contains(defendUnit))
            {
               foreach (var unit in battleStruct.AttackSide.ToList())
               {
                  if (unit == null) continue;
                  if (defenderSector.cellsUnits.Contains(unit))
                  {
                     battleStruct.AttackSide.Remove(unit);
                     unit.GetComponent<IMovable>().ResetDefaults();
                  }
               }

               break;
            }
         }
      }

      BattleInSector battle = new BattleInSector(attackerSector, defenderSector, attackUnits);
      BattlesList.Add(battle);
      foreach (var unit in attackUnits.ToList())
      {
         if (unit == null) continue;
         unit.GetComponent<IBattleSetter>().SetCurrentBattle(battle);
         unit.GetComponent<IAttackSectorSetter>().SetAttackSector(defenderSector);
      }

      foreach (var unit in defenderSector.cellsUnits.ToList())
      {
         if (unit == null) continue;
         unit.GetComponent<IBattleSetter>().SetCurrentBattle(battle);
      }

      SetAttackTargets(battle);
   }

   public void CheckIsNeedToDestroyBattle(BattleInSector battleInSector)
   {
      if (battleInSector.DefendSide.Count == 0 && battleInSector.AttackSide.Count == 0)
      {
         BattlesList.Remove(battleInSector);
      }
   }

   private void SetAttackTargets(BattleInSector battleInSector)
   {
      var attackSide = battleInSector.AttackSide;
      var defendSide = battleInSector.DefendSide;
      var defendersSector = battleInSector.DefendersSector;
      var attackersSector = battleInSector.AttackersSector;
      foreach (var attackUnit in attackSide.ToList())
      {
         if (attackUnit == null) continue;
         if (attackUnit.CompareTag("Infantry"))
         {
            var infantryScript = attackUnit.GetComponent<InfantryScript>();
            ChoosePriorityEnemyForInfantry(infantryScript, battleInSector);
            infantryScript.AttackPosition(defendersSector, attackersSector.cellsUnits.IndexOf(attackUnit));
         }
         else if (attackUnit.CompareTag("LightTank"))
         {
            var tankScript = attackUnit.GetComponent<LightTankScript>();
            ChoosePriorityEnemyForLightTank(tankScript, battleInSector);
            tankScript.AttackPosition(defendersSector, attackersSector.cellsUnits.IndexOf(attackUnit));
         }
         else if (attackUnit.CompareTag("HeavyArtillery"))
         {
            attackUnit.GetComponent<HeavyArtilleryScript>()
               .AttackPosition(defendersSector, attackersSector.cellsUnits.IndexOf(attackUnit));
         }
         else if (attackUnit.CompareTag("HeavyTank"))
         {
            var heavyTankScript = attackUnit.GetComponent<HeavyTankScript>();
            ChoosePriorityEnemyForHeavyTank(heavyTankScript, battleInSector);
            heavyTankScript.AttackPosition(defendersSector, attackersSector.cellsUnits.IndexOf(attackUnit));
         }
      }

      foreach (var defendUnits in defendSide.ToList())
      {
         if (defendUnits == null) continue;
         if (defendUnits.CompareTag("Infantry"))
         {
            ChoosePriorityEnemyForInfantryDefender(defendUnits.GetComponent<InfantryScript>(), battleInSector);
         }
         else if (defendUnits.CompareTag("LightTank"))
         {
            ChoosePriorityEnemyForLightTankDefender(defendUnits.GetComponent<LightTankScript>(), battleInSector);
         }
         else if (defendUnits.CompareTag("HeavyTank"))
         {
            ChoosePriorityEnemyForHeavyTankDefender(defendUnits.GetComponent<HeavyTankScript>(), battleInSector);
         }
         else if (defendUnits.CompareTag("HeavyArtillery"))
         {
            ChoosePriorityEnemyForHeavyTankDefender(defendUnits.GetComponent<HeavyTankScript>(), battleInSector);
         }
      }
   }

   public void ChoosePriorityEnemyForInfantry(InfantryScript infantryScript, BattleInSector battleInSector)
   {
      var attackSide = battleInSector.AttackSide;
      var defendSide = battleInSector.DefendSide;
      var defendersSector = battleInSector.DefendersSector;
      var attackersSector = battleInSector.AttackersSector;
      if (defendSide.Count == 0)
      {
         if (!battleInSector.AttackersSector.cellsUnits.Contains(infantryScript.gameObject))
         {
            infantryScript.GetComponent<IMovable>().SetPositionToSpawnPoint();
            infantryScript.GetComponent<IAttackSectorSetter>().SetAttackSectorToParentSector();
         }
         else
            infantryScript.targetPosition =
               defendersSector.spawnPoints[attackersSector.cellsUnits.IndexOf(infantryScript.gameObject)];
      }
      else
      {
         GameObject enemy = null;
         foreach (var defendUnit in defendSide.ToList())
         {
            if (defendUnit == null) continue;
            if (!defendUnit.CompareTag("Infantry")) continue;
            if (!enemy)
            {
               enemy = defendUnit;
               continue;
            }

            if ((enemy.transform.position - infantryScript.transform.position).magnitude >
                (infantryScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
         }

         foreach (var defendUnit in defendSide.ToList())
         {
            if (defendUnit == null) continue;
            if (enemy) break;
            if (!defendUnit.CompareTag("HeavyArtillery")) continue;
            if (!enemy)
            {
               enemy = defendUnit;
               continue;
            }

            if ((enemy.transform.position - infantryScript.transform.position).magnitude >
                (infantryScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
         }

         foreach (var defendUnit in defendSide.ToList())
         {
            if (defendUnit == null) continue;
            if (enemy) break;
            if (!defendUnit.CompareTag("LightTank")) continue;
            if (!enemy)
            {
               enemy = defendUnit;
               continue;
            }

            if ((enemy.transform.position - infantryScript.transform.position).magnitude >
                (infantryScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
         }

         foreach (var defendUnit in defendSide.ToList())
         {
            if (defendUnit == null) continue;
            if (enemy) break;
            if (!defendUnit.CompareTag("HeavyTank")) continue;
            if (!enemy)
            {
               enemy = defendUnit;
               continue;
            }

            if ((enemy.transform.position - infantryScript.transform.position).magnitude >
                (infantryScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
         }

         if (!infantryScript.targetPosition)
         {
            infantryScript.targetPosition = enemy
               ? enemy.transform
               : defendersSector.spawnPoints[attackersSector.cellsUnits.IndexOf(infantryScript.gameObject)];
         }

         infantryScript.attackTarget = enemy;
      }
   }

   public void ChoosePriorityEnemyForInfantryDefender(InfantryScript infantryScript, BattleInSector battleInSector)
   {
      var attackSide = battleInSector.AttackSide;
      var defendSide = battleInSector.DefendSide;
      var defendersSector = battleInSector.DefendersSector;
      var attackersSector = battleInSector.AttackersSector;
      GameObject enemy = null;
      foreach (var attackUnit in attackSide.ToList())
      {
         if (attackUnit == null) continue;
         if (!attackUnit.CompareTag("Infantry")) continue;
         if (!enemy)
         {
            enemy = attackUnit;
            continue;
         }

         if ((enemy.transform.position - infantryScript.transform.position).magnitude >
             (infantryScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
      }

      foreach (var attackUnit in attackSide)
      {
         if (enemy) break;
         if (!attackUnit.CompareTag("HeavyArtillery")) continue;
         if (!enemy)
         {
            enemy = attackUnit;
            continue;
         }

         if ((enemy.transform.position - infantryScript.transform.position).magnitude >
             (infantryScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
      }

      foreach (var attackUnit in attackSide.ToList())
      {
         if (attackUnit == null) continue;
         if (enemy) break;
         if (!attackUnit.CompareTag("LightTank")) continue;
         if (!enemy)
         {
            enemy = attackUnit;
            continue;
         }

         if ((enemy.transform.position - infantryScript.transform.position).magnitude >
             (infantryScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
      }

      foreach (var defendUnit in defendSide.ToList())
      {
         if (defendUnit == null) continue;
         if (enemy) break;
         if (!defendUnit.CompareTag("HeavyTank")) continue;
         if (!enemy)
         {
            enemy = defendUnit;
            continue;
         }

         if ((enemy.transform.position - infantryScript.transform.position).magnitude >
             (infantryScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
      }

      infantryScript.attackTarget = enemy;
   }

   public void ChoosePriorityEnemyForLightTank(LightTankScript lightTankScript, BattleInSector battleInSector)
   {
      var attackSide = battleInSector.AttackSide;
      var defendSide = battleInSector.DefendSide;
      var defendersSector = battleInSector.DefendersSector;
      var attackersSector = battleInSector.AttackersSector;
      var idxOfUnit = attackersSector.cellsUnits.IndexOf(lightTankScript.gameObject);
      if (defendSide.Count == 0)
      {
         if (!battleInSector.AttackersSector.cellsUnits.Contains(lightTankScript.gameObject))
         {
            if (battleInSector.AttackSide.Contains(lightTankScript.gameObject))
               battleInSector.AttackSide.Remove(lightTankScript.gameObject);
            lightTankScript.GetComponent<IMovable>().SetPositionToSpawnPoint();
            lightTankScript.GetComponent<IAttackSectorSetter>().SetAttackSectorToParentSector();
         }
         else
         {
            if (idxOfUnit > 3 || idxOfUnit < 0)
            {
               Debug.Log("Im gay");
            }

            lightTankScript.targetPosition =
               defendersSector.spawnPoints[attackersSector.spawnPoints.IndexOf(lightTankScript.transform.parent)];
         }

         lightTankScript.attackTarget = null;
      }
      else
      {
         GameObject enemy = null;
         foreach (var defendUnit in defendSide.ToList())
         {
            if (defendUnit == null) continue;
            if (!defendUnit.CompareTag("LightTank")) continue;
            if (!enemy)
            {
               enemy = defendUnit;
               continue;
            }

            if ((enemy.transform.position - lightTankScript.transform.position).magnitude >
                (lightTankScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
         }

         foreach (var defendUnit in defendSide.ToList())
         {
            if (defendUnit == null) continue;
            if (!defendUnit.CompareTag("HeavyTank")) continue;
            if (!enemy)
            {
               enemy = defendUnit;
               continue;
            }

            if ((enemy.transform.position - lightTankScript.transform.position).magnitude >
                (lightTankScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
         }

         if (!enemy)
            foreach (var defendUnit in defendSide.ToList())
            {
               if (defendUnit == null) continue;
               if (!defendUnit.CompareTag("HeavyArtillery")) continue;
               if (!enemy)
               {
                  enemy = defendUnit;
                  continue;
               }

               if ((enemy.transform.position - lightTankScript.transform.position).magnitude >
                   (lightTankScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
            }

         if (!enemy)
            foreach (var defendUnit in defendSide.ToList())
            {
               if (defendUnit == null) continue;
               if (!defendUnit.CompareTag("Infantry")) continue;
               if (!enemy)
               {
                  enemy = defendUnit;
                  continue;
               }

               if ((enemy.transform.position - lightTankScript.transform.position).magnitude >
                   (lightTankScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
            }

         if (!enemy) return;
         if (!lightTankScript.targetPosition)
         {
            lightTankScript.targetPosition = enemy
               ? enemy.transform
               : defendersSector.spawnPoints[idxOfUnit];
         }

         lightTankScript.attackTarget = enemy;
         lightTankScript.targetPosition = enemy.transform;
      }
   }

   public void ChoosePriorityEnemyForLightTankDefender(LightTankScript lightTankScript, BattleInSector battleInSector)
   {
      var attackSide = battleInSector.AttackSide;
      var defendSide = battleInSector.DefendSide;
      var defendersSector = battleInSector.DefendersSector;
      var attackersSector = battleInSector.AttackersSector;
      GameObject enemy = null;
      foreach (var attackUnit in attackSide.ToList())
      {
         if (attackUnit == null) continue;
         if (!attackUnit.CompareTag("LightTank")) continue;
         if (!enemy)
         {
            enemy = attackUnit;
            continue;
         }

         if ((enemy.transform.position - lightTankScript.transform.position).magnitude >
             (lightTankScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
      }

      foreach (var attackUnit in attackSide.ToList())
      {
         if (attackUnit == null) continue;
         if (!attackUnit.CompareTag("HeavyTank")) continue;
         if (!enemy)
         {
            enemy = attackUnit;
            continue;
         }

         if ((enemy.transform.position - lightTankScript.transform.position).magnitude >
             (lightTankScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
      }

      if (!enemy)
         foreach (var attackUnit in attackSide.ToList())
         {
            if (attackUnit == null) continue;
            if (!attackUnit.CompareTag("HeavyArtillery")) continue;
            if (!enemy)
            {
               enemy = attackUnit;
               continue;
            }

            if ((enemy.transform.position - lightTankScript.transform.position).magnitude >
                (lightTankScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
         }

      if (!enemy)
         foreach (var attackUnit in attackSide)
         {
            if (!attackUnit.CompareTag("Infantry")) continue;
            if (!enemy)
            {
               enemy = attackUnit;
               continue;
            }

            if ((enemy.transform.position - lightTankScript.transform.position).magnitude >
                (lightTankScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
         }

      lightTankScript.attackTarget = enemy;
   }

   public void ChoosePriorityEnemyForHeavyTank(HeavyTankScript heavyTankScript, BattleInSector battleInSector)
   {
      var attackSide = battleInSector.AttackSide;
      var defendSide = battleInSector.DefendSide;
      var defendersSector = battleInSector.DefendersSector;
      var attackersSector = battleInSector.AttackersSector;
      var idxOfUnit = attackersSector.cellsUnits.IndexOf(heavyTankScript.gameObject);
      if (defendSide.Count == 0)
      {
         if (!battleInSector.AttackersSector.cellsUnits.Contains(heavyTankScript.gameObject))
         {
            if (battleInSector.AttackSide.Contains(heavyTankScript.gameObject))
               battleInSector.AttackSide.Remove(heavyTankScript.gameObject);
            heavyTankScript.GetComponent<IMovable>().SetPositionToSpawnPoint();
            heavyTankScript.GetComponent<IAttackSectorSetter>().SetAttackSectorToParentSector();
         }
         else
         {
            if (idxOfUnit > 3 || idxOfUnit < 0)
            {
               Debug.Log("Im gay");
            }
            heavyTankScript.targetPosition =
               defendersSector.spawnPoints[attackersSector.spawnPoints.IndexOf(heavyTankScript.transform.parent)];
         }

         heavyTankScript.attackTarget = null;
      }
      else
      {
         GameObject enemy = null;
         foreach (var defendUnit in defendSide.ToList())
         {
            if (defendUnit == null) continue;
            if (!defendUnit.CompareTag("HeavyTank")) continue;
            if (!enemy)
            {
               enemy = defendUnit;
               continue;
            }

            if ((enemy.transform.position - heavyTankScript.transform.position).magnitude >
                (heavyTankScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
         }

         foreach (var defendUnit in defendSide.ToList())
         {
            if (defendUnit == null) continue;
            if (!defendUnit.CompareTag("LightTank")) continue;
            if (!enemy)
            {
               enemy = defendUnit;
               continue;
            }

            if ((enemy.transform.position - heavyTankScript.transform.position).magnitude >
                (heavyTankScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
         }

         if (!enemy)
            foreach (var defendUnit in defendSide.ToList())
            {
               if (defendUnit == null) continue;
               if (!defendUnit.CompareTag("HeavyArtillery")) continue;
               if (!enemy)
               {
                  enemy = defendUnit;
                  continue;
               }

               if ((enemy.transform.position - heavyTankScript.transform.position).magnitude >
                   (heavyTankScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
            }

         if (!enemy)
            foreach (var defendUnit in defendSide.ToList())
            {
               if (defendUnit == null) continue;
               if (!defendUnit.CompareTag("Infantry")) continue;
               if (!enemy)
               {
                  enemy = defendUnit;
                  continue;
               }

               if ((enemy.transform.position - heavyTankScript.transform.position).magnitude >
                   (heavyTankScript.transform.position - defendUnit.transform.position).magnitude) enemy = defendUnit;
            }

         if (!enemy) return;
         if (!heavyTankScript.targetPosition)
         {
            heavyTankScript.targetPosition = enemy
               ? enemy.transform
               : defendersSector.spawnPoints[idxOfUnit];
         }

         heavyTankScript.attackTarget = enemy;
         heavyTankScript.targetPosition = enemy.transform;
      }
   }

   public void ChoosePriorityEnemyForHeavyTankDefender(HeavyTankScript heavyTankScript, BattleInSector battleInSector)
   {
      var attackSide = battleInSector.AttackSide;
      var defendSide = battleInSector.DefendSide;
      var defendersSector = battleInSector.DefendersSector;
      var attackersSector = battleInSector.AttackersSector;
      GameObject enemy = null;
      foreach (var attackUnit in attackSide.ToList())
      {
         if (attackUnit == null) continue;
         if (!attackUnit.CompareTag("HeavyTank")) continue;
         if (!enemy)
         {
            enemy = attackUnit;
            continue;
         }

         if ((enemy.transform.position - heavyTankScript.transform.position).magnitude >
             (heavyTankScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
      }

      foreach (var attackUnit in attackSide.ToList())
      {
         if (attackUnit == null) continue;
         if (enemy) break;
         if (!attackUnit.CompareTag("LightTank")) continue;
         if (!enemy)
         {
            enemy = attackUnit;
            continue;
         }

         if ((enemy.transform.position - heavyTankScript.transform.position).magnitude >
             (heavyTankScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
      }

      if (!enemy)
         foreach (var attackUnit in attackSide.ToList())
         {
            if (attackUnit == null) continue;
            if (!attackUnit.CompareTag("HeavyArtillery")) continue;
            if (!enemy)
            {
               enemy = attackUnit;
               continue;
            }

            if ((enemy.transform.position - heavyTankScript.transform.position).magnitude >
                (heavyTankScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
         }

      if (!enemy)
         foreach (var attackUnit in attackSide)
         {
            if (!attackUnit.CompareTag("Infantry")) continue;
            if (!enemy)
            {
               enemy = attackUnit;
               continue;
            }

            if ((enemy.transform.position - heavyTankScript.transform.position).magnitude >
                (heavyTankScript.transform.position - attackUnit.transform.position).magnitude) enemy = attackUnit;
         }

      heavyTankScript.attackTarget = enemy;
   }

   private void IterateBattle()
   {
      foreach (var battleInSector in BattlesList.ToList())
      {
         var attackSide = battleInSector.AttackSide;
         var defendSide = battleInSector.DefendSide;
         var defendersSector = battleInSector.DefendersSector;
         var attackersSector = battleInSector.AttackersSector;
         foreach (var defendUnit in defendSide.ToList())
         {
            if (defendUnit == null)
            {
               defendSide.Remove(defendUnit);
               continue;
            }

            if (defendUnit.CompareTag("Infantry"))
            {
               var script = defendUnit.GetComponent<InfantryScript>();
               script.ShootingInTarget();
            }
            else if (defendUnit.CompareTag("LightTank"))
            {
               defendUnit.GetComponent<LightTankScript>().ShootingInTarget();
            }
            else if (defendUnit.CompareTag("HeavyTank"))
            {
               defendUnit.GetComponent<HeavyTankScript>().ShootingInTarget();
            }
         }

         foreach (var attackUnit in attackSide.ToList())
         {
            if (attackUnit == null)
            {
               attackSide.Remove(attackUnit);
               continue;
            }

            if (attackUnit.CompareTag("Infantry"))
            {
               var script = attackUnit.GetComponent<InfantryScript>();
               script.ShootingInTarget();
            }
            else if (attackUnit.CompareTag("LightTank"))
            {
               attackUnit.GetComponent<LightTankScript>().ShootingInTarget();
            }
            else if (attackUnit.CompareTag("HeavyTank"))
            {
               attackUnit.GetComponent<HeavyTankScript>().ShootingInTarget();
            }
         }

         // экспериментальный вариант
         CheckIsNeedToDestroyBattle(battleInSector);
         //
         if (defendSide.Count == 0)
         {
            foreach (var attackUnit in attackSide.ToList())
            {
               if (attackUnit == null) continue;
               if (attackUnit.CompareTag("Infantry"))
               {
                  var infantryScript = attackUnit.GetComponent<InfantryScript>();
                  ChoosePriorityEnemyForInfantry(infantryScript, battleInSector);
               }
               else if (attackUnit.CompareTag("LightTank"))
               {
                  var lightTankScript = attackUnit.GetComponent<LightTankScript>();
                  ChoosePriorityEnemyForLightTank(lightTankScript, battleInSector);
               }
               else if (attackUnit.CompareTag("HeavyTank"))
               {
                  var heavyTankScript = attackUnit.GetComponent<HeavyTankScript>();
                  ChoosePriorityEnemyForHeavyTank(heavyTankScript, battleInSector);
               }
            }
         }
         else if (attackSide.Count == 0)
         {
            foreach (var defendUnit in defendSide.ToList())
            {
               if (defendUnit == null) continue;
               if (defendUnit.CompareTag("Infantry"))
               {
                  var infantryScript = defendUnit.GetComponent<InfantryScript>();
                  infantryScript.ResetDefaults();
                  infantryScript.currentBattle.ClearBattle();
               }
               else if (defendUnit.CompareTag("LightTank"))
               {
                  var lightTankScript = defendUnit.GetComponent<LightTankScript>();
                  lightTankScript.ResetDefaults();
                  lightTankScript.currentBattle.ClearBattle();
               }
               else if (defendUnit.CompareTag("HeavyTank"))
               {
                  var heavyTankScript = defendUnit.GetComponent<HeavyTankScript>();
                  heavyTankScript.ResetDefaults();
                  heavyTankScript.currentBattle.ClearBattle();
               }
            }

            BattlesList.Remove(battleInSector);
         }
      }

      _time = 0;
   }

   private void FixedUpdate()
   {
      if(!CanvasesManager.IsGameStarted) return;
      _time += Time.fixedDeltaTime;
      if (_time >= 1)
      {
         IterateBattle();
      }
   }

   private void Awake()
   {
      Current = this;
   }

   public bool CheckIsSectorInAnyBattle(SectorInfo sectorInfo)
   {

      foreach (var battle in BattlesList)
      {
         if (sectorInfo == battle.AttackersSector || sectorInfo == battle.DefendersSector)
         {
            return true;
         }

         foreach (var unit in sectorInfo.cellsUnits)
         {
            if (battle.AttackSide.Contains(unit) || battle.DefendSide.Contains(unit)) return true;
         }
      }

      return false;
   }

   private bool CheckIsSectorWithoutUnitsInAnyBattle(SectorInfo sectorInfo)
   {
      foreach (var neighbour in sectorInfo.neighbours)
      {
         if (neighbour.isPlayerOwner == sectorInfo.isPlayerOwner) continue;
         foreach (var unit in neighbour.cellsUnits)
         {
            if (sectorInfo.spawnPoints.Contains(unit.GetComponent<IMovable>().ReturnTargetPosition())) return true;
         }
      }

      return false;
   }
}
