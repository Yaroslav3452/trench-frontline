﻿using UnityEngine;

namespace Interfaces
{
    public interface IMovable
    {
        void MoveToAllySector(SectorInfo sectorInfo, int cellPosition);
        void SetPositionToSpawnPoint();
        void RemoveUnit();
        void ResetDefaults();
        Transform ReturnTargetPosition();
        float ReturnPercentageHpOfUnit();

        void HpRegeneration();
    }
    public interface IAttack
    {
        void AttackPosition(SectorInfo sectorInfo, int cellPosition);
    }
    public interface IBattleSetter
    {
        void SetCurrentBattle(Battle.BattleInSector battleInSector);
    }

    public interface IAttackSectorSetter
    {
        void SetAttackSector(SectorInfo sectorInfo);
        void SetAttackSectorToParentSector();
    }

    public interface IAIinformationSet
    {
        float ReturnThreat();
        int ReturnTypeOfUnit();
    }
    
}