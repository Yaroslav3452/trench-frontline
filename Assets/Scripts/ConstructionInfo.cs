﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

public class ConstructionInfo : MonoBehaviour
{
    public List<Transform> spawnPointsForBunkers = new List<Transform>();
    public List<Transform> spawnPointsForTrenches = new List<Transform>();
    public List<Transform> spawnPointsForResourcesExtractors = new List<Transform>();
    [SerializeField] private List<GameObject> bunkerPrefab = new List<GameObject>();
    [SerializeField] private GameObject trenchPrefab;
    [SerializeField] private GameObject resourceExtractorPrefab;
    private static Vector3 targetSizeForBunkers = new Vector3(1f,1f,1f);
    [SerializeField] private Vector3 targetSizeForDecrease = Vector3.zero;
    private SectorInfo _sectorInfo;
    private List<Transform> _oldBlocksForScalingDecreasing = new List<Transform>();
    private List<Transform> _newBlocksForScalingIncreasing = new List<Transform>();

    public void BuildBunker()
    {
        BlockInfo prevblock = null;
        Transform buildPoint = null;
        foreach (var point in spawnPointsForBunkers)
        {
            if (point.childCount == 1 && point.GetChild(0).CompareTag("GroundBlock"))
            {
                buildPoint = point;
                prevblock = point.GetChild(0).GetComponent<BlockInfo>();
                _oldBlocksForScalingDecreasing.Add(point.GetChild(0));
                break;
            }
        }

        if (!(buildPoint is null))
        {
            var random = new Random();
            var modelidx = random.Next(0,3);
            var bunker = Instantiate(bunkerPrefab[modelidx], buildPoint);
            bunker.transform.localScale = Vector3.zero;
            _newBlocksForScalingIncreasing.Add(bunker.transform);
            var parentGroupBlocks = prevblock.GetComponentInParent<SectorInfo>();
            var idxOfPrevBlock = parentGroupBlocks.blockInfos.IndexOf(prevblock);
            parentGroupBlocks.blockInfos[idxOfPrevBlock] = bunker.GetComponentInChildren<BlockInfo>();
            if(!_sectorInfo.isPlayerOwner) RotateBunker(bunker);
        }
    }
    public void BuildTrench()
    {
        BlockInfo prevblock = null;
        Transform buildPoint = null;
        foreach (var point in spawnPointsForTrenches)
        {
            if (point.childCount == 1 && point.GetChild(0).CompareTag("GroundBlock"))
            {
                buildPoint = point;
                prevblock = point.GetChild(0).GetComponent<BlockInfo>();
                _oldBlocksForScalingDecreasing.Add(point.GetChild(0));
                break;
            }
        }

        if (!(buildPoint is null))
        {
            var trench = Instantiate(trenchPrefab, buildPoint);
            trench.transform.localScale = Vector3.zero;
            _newBlocksForScalingIncreasing.Add(trench.transform);
            var parentGroupBlocks = prevblock.GetComponentInParent<SectorInfo>();
            var idxOfPrevBlock = parentGroupBlocks.blockInfos.IndexOf(prevblock);
            parentGroupBlocks.blockInfos[idxOfPrevBlock] = trench.GetComponentInChildren<BlockInfo>();
        }
    }
    public void BuildResources()
    {
        Transform buildPoint = null;
        foreach (var point in spawnPointsForResourcesExtractors)
        {
            if (point.childCount == 0)
            {
                buildPoint = point;
                break;
            }
        }

        if (!(buildPoint is null))
        {
            var resourceExtractor = Instantiate(resourceExtractorPrefab, buildPoint);
            resourceExtractor.transform.localScale = Vector3.zero;
            _newBlocksForScalingIncreasing.Add(resourceExtractor.transform);
        }
    }

    private void RotateBunker(GameObject bunker)
    {
        var rotation = transform.rotation;
        bunker.transform.rotation = new Quaternion(rotation.x, 180, rotation.z, rotation.w);
    }
    public void RotateAllBunkersInSector()
    {
        foreach (var point in spawnPointsForBunkers)
        {
            if (point.childCount == 1 && point.GetChild(0).CompareTag("BunkerBlock"))
            {
                RotateBunker(point.GetChild(0).gameObject);
            }
            if (point.childCount == 2 && point.GetChild(1).CompareTag("BunkerBlock"))
            {
                RotateBunker(point.GetChild(1).gameObject);
            }
        }
    }

    private void Start()
    {
        _sectorInfo = gameObject.GetComponent<SectorInfo>();
    }

    private void Update()
    {
        if(!CanvasesManager.IsGameStarted) return;
        foreach (var block in _oldBlocksForScalingDecreasing.ToList())
        {
            block.transform.localScale = Vector3.Lerp(block.transform.localScale, targetSizeForDecrease, 1f * Time.deltaTime);
            if (block.transform.localScale.magnitude <= new Vector3(0.1f, 0.1f, 0.1f).magnitude)
            {
                _oldBlocksForScalingDecreasing.Remove(block);
                Destroy(block.gameObject);
            }
        }

        foreach (var block in _newBlocksForScalingIncreasing)
        {
            block.transform.localScale = Vector3.Lerp(block.transform.localScale, targetSizeForBunkers, 1f * Time.deltaTime);
        }
    }
}