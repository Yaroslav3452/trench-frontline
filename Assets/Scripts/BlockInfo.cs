﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockInfo : MonoBehaviour
{
   public MeshRenderer meshRenderer;
   public Color currentDefaultColor;
   public Color defaultColor;
   public Color defaultEnemyColor;
   public Transform unitSpawnPoint;
   public Color targetColor;

   private void Awake()
   {
      meshRenderer = GetComponent<MeshRenderer>();
      defaultColor = meshRenderer.material.color;
      currentDefaultColor = defaultColor;
      targetColor = currentDefaultColor;
   }
}
