﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Interfaces;
using UnityEngine;

public class AiInfo : MonoBehaviour
{
    public static AiInfo Current;
    public enum UnitType
    {
        Infantry = 1,
        HeavyArty,
        AntiTank,
        LightTank,
        HeavyTank,
    }
    public List<SectorInfo> resourceSilos = new List<SectorInfo>();
    public float resourcesCount = 0;
    public int resourceIncome;
    public float aiResorcesMultiple = 1f;
    public List<SectorInfo> aiSectors = new List<SectorInfo>();
    [SerializeField] private bool isAiWorking = true;
    [SerializeField] private GameObject groundParent;
    [SerializeField] private bool heavyTanksResearched = false;
    [SerializeField] private bool lightTanksResearched = false;
    private float _timer;
    private float _timerForAttack;
    private List<SectorInfo> _fronlineSectors = new List<SectorInfo>();
    private float _reserveResourcesCount = 300;
    private float aiResearchTimer = 90f;


    public void GetAISectors()
    {
        int countOfPlayerSectors = 0;
        aiSectors.Clear();
        var sectors = groundParent.GetComponentsInChildren<SectorInfo>();
        foreach (var sector in sectors)
        {
            if (!sector.isPlayerOwner && !aiSectors.Contains(sector)) aiSectors.Add(sector);
            else if(sector.isPlayerOwner) countOfPlayerSectors++;
        }

        if (aiSectors.Count == 0)
        {
            CanvasesManager.Current.UWin();
        }

        if (countOfPlayerSectors == 0)
        {
            CanvasesManager.Current.Ulose();
        }
    }

    private void ReinforceSector()
    {
        var sectorForReinforce = CalcPrioritySectorForReinforcements();
        if(sectorForReinforce == null) return;
        if(Battle.Current.CheckIsSectorInAnyBattle(sectorForReinforce)) return;
        if (sectorForReinforce.cellsUnits.Count < 3)
        {
            CalcBetterUnitForCreating(sectorForReinforce);
        }

        if (sectorForReinforce.trenchLevel < 3)
        {
            for (int i = sectorForReinforce.fortificationLevel; i < 3; i++)
            {
                SectorPanel.Current.FortificationSectorTrench(sectorForReinforce);
            }
        }
        if (sectorForReinforce.fortificationLevel < 3)
        {
            for (int i = sectorForReinforce.fortificationLevel; i < 3; i++)
            {
                SectorPanel.Current.FortificationSectorBunker(sectorForReinforce);
            }
        }
    }
    
    /// <summary>
/// Считает какой тип юнитов лучше подойдет для защиты сектора
/// </summary>
/// <param name="sectorInfo"> сектор бота</param>
    private void CalcBetterUnitForCreating(SectorInfo sectorInfo)
    {
        var mechanizedThreat = 0f;
        var infantryThreat = 0f;
        foreach (var neighbour in sectorInfo.neighbours)
        {
            if (!neighbour.isPlayerOwner) continue;
            foreach (var unit in neighbour.cellsUnits)
            {
                var type = unit.GetComponent<IAIinformationSet>().ReturnTypeOfUnit();
                switch (type)
                {
                    case (int) UnitType.HeavyTank:
                    case (int) UnitType.LightTank:
                        mechanizedThreat += unit.GetComponent<IAIinformationSet>().ReturnThreat();
                        break;
                    case (int) UnitType.AntiTank:
                    case (int) UnitType.HeavyArty:
                    case (int) UnitType.Infantry:
                        infantryThreat += unit.GetComponent<IAIinformationSet>().ReturnThreat();
                        break;
                }
            }
        }
        if (mechanizedThreat == 0 && infantryThreat == 0)
        {
            //if(resourcesCount > SectorPanel.Current.heavyTankCost) SectorPanel.Current.AddLightTank();
            if(resourcesCount > SectorPanel.Current.lightTankCost) SectorPanel.Current.AddLightTank(sectorInfo);
            if(resourcesCount > SectorPanel.Current.lightTankCost) SectorPanel.Current.AddLightTank(sectorInfo);
            if(resourcesCount > SectorPanel.Current.lightTankCost) SectorPanel.Current.AddLightTank(sectorInfo);
        } else
            for (int i = sectorInfo.cellsUnits.Count; i < 3; i++)
            {
                if (mechanizedThreat > infantryThreat)
                {
                    if (heavyTanksResearched && resourcesCount > SectorPanel.Current.heavyTankCost)
                    {
                        SectorPanel.Current.AddHeavyTank(sectorInfo);
                    }

                    if (lightTanksResearched && resourcesCount > SectorPanel.Current.lightTankCost)
                    {
                        SectorPanel.Current.AddLightTank(sectorInfo);
                    }
                }
                else
                {
                    if (resourcesCount > SectorPanel.Current.infantryCost)
                    {
                        SectorPanel.Current.AddInfantry(sectorInfo);
                    }

                    if (lightTanksResearched && resourcesCount > SectorPanel.Current.lightTankCost)
                    {
                        SectorPanel.Current.AddLightTank(sectorInfo);
                    }
                }
            }
    }

    /// <summary>
    /// Для всех секторов линии фронта ищет самый уязвимый для атаки
    /// </summary>
    /// <returns>сектор для укрепления</returns>
    private SectorInfo CalcPrioritySectorForReinforcements()
    {
        AddLvlToResourceSilo();
        SectorInfo sectorForPriorityReinforcement = null;
        var priorityThreat = 0f;
        foreach (var fronlineSector in _fronlineSectors)
        {
            if (sectorForPriorityReinforcement == null)
            {
                sectorForPriorityReinforcement = fronlineSector;
                priorityThreat = CalcSectorThreat(fronlineSector);
                continue;
            }
            var sectorThreat = CalcSectorThreat(fronlineSector);
            if(priorityThreat - CalcSectorPower(sectorForPriorityReinforcement) <
               sectorThreat - CalcSectorPower(fronlineSector) &&
               (fronlineSector.trenchLevel < 3 || fronlineSector.fortificationLevel < 3 || fronlineSector.cellsUnits.Count < 3))
            {
                priorityThreat = sectorThreat;
                sectorForPriorityReinforcement = fronlineSector;
            }
        }
        return sectorForPriorityReinforcement;
    }

/// <summary>
/// Считает угрозу от соседних клеток по юнитам в них
/// </summary>
/// <param name="sectorInfo">сектор</param>
/// <returns>численная угроза от юнитов</returns>
    private float CalcSectorThreat(SectorInfo sectorInfo)
    {
        float threat = 0;
        if(!sectorInfo.isPlayerOwner)
        {
            foreach (var neighbour in sectorInfo.neighbours)
            {
                if (!neighbour.isPlayerOwner) continue;
                threat += CalcSectorPower(neighbour);
            }
        }
        else
        {
            foreach (var neighbour in sectorInfo.neighbours)
            {
                if (neighbour.isPlayerOwner) continue;
                threat += CalcSectorPower(neighbour);
            }
        }
        return threat;
    }

    /// <summary>
/// Считает силу одного сектора по юнитам
/// </summary>
/// <param name="sectorInfo">сектор</param>
/// <returns>float - силу сектора</returns>
private float CalcSectorPower(SectorInfo sectorInfo)
{
    float threat = 0;
    foreach (var unit in sectorInfo.cellsUnits.ToList())
    {
        if(unit == null) continue;
        threat += unit.GetComponent<IAIinformationSet>().ReturnThreat();
    }
    return threat;
}

private void CalcSectorForAttack()
{
    SectorInfo priorityTarget = null;
    foreach (var fronlineSector in _fronlineSectors)
    {
        foreach (var neighbour in fronlineSector.neighbours)
        {
            if (neighbour.isPlayerOwner)
            {
                if (priorityTarget == null)
                {
                    priorityTarget = neighbour;
                    continue;
                }
                if (CalcSectorThreat(priorityTarget) < CalcSectorThreat(neighbour))
                {
                    priorityTarget = neighbour;
                }
            }
        }
    }
    if(priorityTarget == null) return;
    if(resourcesCount < _reserveResourcesCount * 2) return; //проверка есть ли у бота ресурсы в запасе
    //if(Battle.Current.CheckIsSectorInAnyBattle(priorityTarget)) return;
    foreach (var neighbour in priorityTarget.neighbours)
    {
        if (!neighbour.isPlayerOwner && neighbour.cellsUnits.Count > 0 && !Battle.Current.CheckIsSectorInAnyBattle(neighbour))
        {
            SectorPanel.Current.AttackSector(neighbour, priorityTarget);
        }
    }
    //attention! may not work well.
    if (Battle.Current.CheckIsSectorInAnyBattle(priorityTarget))
    {
        foreach (var unit in priorityTarget.cellsUnits)
        {
            unit.GetComponent<IMovable>().ResetDefaults();
        }
    }
    if (priorityTarget != null) Debug.Log(priorityTarget.gameObject.name);
}

private List<SectorInfo> CalcEnemySectorsNearSector(SectorInfo sectorInfo)
{
    List<SectorInfo> sectorsNearTarget = new List<SectorInfo>();
    if (sectorInfo.isPlayerOwner)
    {
        foreach (var neighbour in sectorInfo.neighbours)
        {
            if (!neighbour.isPlayerOwner) sectorsNearTarget.Add(neighbour);
        }
    }
    else
    {
        foreach (var neighbour in sectorInfo.neighbours)
        {
            if (neighbour.isPlayerOwner) sectorsNearTarget.Add(neighbour);
        } 
    }

    return sectorsNearTarget;
}

/// <summary>
/// ищет сектора, граничащие с игроком
/// </summary>
    private void FindFrontlineSectors()
    {
        _fronlineSectors.Clear();
        foreach (var aiSector in aiSectors)
        {
            foreach (var neighbour in aiSector.neighbours)
            {
                if (neighbour.isPlayerOwner)
                {
                    if(!_fronlineSectors.Contains(aiSector)) _fronlineSectors.Add(aiSector);
                    break;
                }
            }
        }
    }

private void AddLvlToResourceSilo()
{
    if(resourcesCount < _reserveResourcesCount * 2) return;
    foreach (var silo in resourceSilos)
    {
        if (silo.resourcesLevel < 5)
        {
            SectorPanel.Current.UpgradeResources(silo);
            return;
        }
    }
}

private void FindSectorsWithoutNeighbours()
{
    List<SectorInfo> sectorsWithoutEnemy = new List<SectorInfo>();
    foreach (var aiSector in aiSectors.ToList())
    {
        bool isEnemynear = false;
        foreach (var neighbour in aiSector.neighbours)
        {
            if (neighbour.isPlayerOwner) isEnemynear = true;
        }
        if(!isEnemynear) sectorsWithoutEnemy.Add(aiSector);
    }

    foreach (var sectorInfo in sectorsWithoutEnemy)
    {
        SectorPanel.Current.RemoveUnits(sectorInfo);
    }
}

private void FixedUpdate()
    {
        if(!CanvasesManager.IsGameStarted) return;
        if(!isAiWorking) return;
        aiResearchTimer -= Time.fixedDeltaTime;
        if (aiResearchTimer <= 0)
        {
            ResearchScript.Current.ResearchForAi();
            aiResearchTimer = 90f;
        }
        _timer += Time.fixedDeltaTime;
        _timerForAttack += Time.fixedDeltaTime;
        if (_timer >= 2f)
        {
            AddResources();
            GetAISectors();
            FindFrontlineSectors();
            ReinforceSector();
            //FindSectorsWithoutNeighbours();
            _timer = 0;
        }

        if (_timerForAttack >= 5f)
        {
            CalcSectorForAttack();
            _timerForAttack = 0;
        }
    }

private void AddResources()
    {
        var income = 0f;
        foreach (var resourceSilo in resourceSilos)
        {
            income += resourceSilo.resourcesLevel * resourceIncome;
        }
        resourcesCount += income * aiResorcesMultiple;
    }

    private void Start()
    {
        GetAISectors();
    }

    private void Awake()
    {
        Current = this;
    }
}
