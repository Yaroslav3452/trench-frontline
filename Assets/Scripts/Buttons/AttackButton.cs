﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class AttackButton : MonoBehaviour
{
    private Transform _transform;

    private void OnMouseDown()
    {
        if(TouchPad.Current.IsPointerOverUIObject(Input.mousePosition)) return;
        var sectorInfo = gameObject.GetComponentInParent<SectorInfo>();
        switch (sectorInfo.isPlayerOwner)
        {
            case false when gameObject.CompareTag("AttackButton"):
                SectorPanel.Current.AttackSector(null, sectorInfo);
                break;
            case false when gameObject.CompareTag("AttackButtonForArty"):
                SectorPanel.Current.AttackSectorViaArty(sectorInfo);
                break;
            case true:
                if(!Battle.Current.CheckIsSectorInAnyBattle(sectorInfo)) SectorPanel.Current.RelocateToAllySector(sectorInfo);
                break;
        }

        CanvasesManager.Current.CloseSectorInfo();
    }

    private void Start()
    {
        _transform = transform;
    }
}
