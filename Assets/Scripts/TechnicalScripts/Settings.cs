﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace TechnicalScripts
{
    public class Settings : MonoBehaviour
    {
        public static Settings Current;
        public float hpRegenerationPercent;
        public int vibration;
        public int qualitySettings;
        public int volume;
        public int difficulty;
        [SerializeField] private Slider volumeSlider;
        [SerializeField] private Slider difficultySlider;

        private void Awake()
        {
            Current = this;
            LoadGameSettings();
        }

        public void SetUIValuesOnStart()
        {
            var difficultyTemp = difficulty;
            volumeSlider.value = volume;
            difficultySlider.value = difficultyTemp;
            CanvasesManager.Current.SetQualityText();
            CanvasesManager.Current.SetVibrationText();
            SoundManager.Current.SetMasterVolume();
        }

        public void CheckSliderValues()
        {
            SoundManager.Current.SetMasterVolume();
            volume = (int) volumeSlider.value;
            difficulty = (int) difficultySlider.value;
            AiInfo.Current.aiResorcesMultiple = 1f + (difficulty / 100f);
        }

        public void SaveGameSettings()
        {
            PlayerPrefs.SetInt("SavedVibration", vibration);
            PlayerPrefs.SetInt("SavedQuality", qualitySettings);
            PlayerPrefs.SetInt("SavedVolume", volume);
            PlayerPrefs.SetInt("Difficulty", difficulty);
        }

        public void ChangeQuality()
        {
            if (qualitySettings == 1)
            {
                QualitySettings.SetQualityLevel(0);
                qualitySettings = 0;
            }
            else
            {
                QualitySettings.SetQualityLevel(1);
                qualitySettings = 1;
            }

            CanvasesManager.Current.SetQualityText();
        }

        public void VibrationChanger()
        {
            if (vibration == 1)
            {
                vibration = 0;
            }
            else
            {
                vibration = 1;
            }

            CanvasesManager.Current.SetVibrationText();
        }

        private void LoadGameSettings()
        {
            if (PlayerPrefs.HasKey("SavedVibration")) vibration = PlayerPrefs.GetInt("SavedVibration");
            if (PlayerPrefs.HasKey("SavedQuality")) qualitySettings = PlayerPrefs.GetInt("SavedQuality");
            if (PlayerPrefs.HasKey("Difficulty")) difficulty = PlayerPrefs.GetInt("Difficulty");
            if (PlayerPrefs.HasKey("SavedVolume")) volume = PlayerPrefs.GetInt("SavedVolume");
            SetUIValuesOnStart();
        }
    }
}
