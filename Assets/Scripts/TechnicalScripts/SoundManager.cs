﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace TechnicalScripts
{
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager Current;
        public AudioSource menuSoundSource;
        public AudioClip lightTankShot;
        public AudioClip heavyTankShot;
        public AudioClip artyShot;
        public AudioClip infantryShot;
        public AudioClip infantryMoving;
        public AudioClip mechanizedMoving;
        public AudioClip clickInMenuSound;
        public List<AudioClip> backgroundMusic = new List<AudioClip>();
        [SerializeField] private AudioMixer _audioMixer;
        public void ClickInMenu()
        {
            menuSoundSource.Play();
        }
        public void SetMasterVolume()
        {
            _audioMixer.SetFloat("Master", -40 + Settings.Current.volume);
        }

        private void Awake()
        {
            Current = this;
        }
    }
}
