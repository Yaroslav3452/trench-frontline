﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class TouchPad : MonoBehaviour, IPointerDownHandler, IPointerUpHandler,IDragHandler
{
    public static TouchPad Current;
    public GameObject cinemachineCamera;
    public Vector3 originDrag;
    public float distance;
    public Vector3 directionDrag;
    public Vector3 currentPosition;
    public Vector3 targetCameraCoordinates;
    [SerializeField] private float bottomBorder = 15f;
    [SerializeField] private float topBorder = 10f;
    private Transform _cameraTransform;
    private Vector3 _cameraPositionStart;
    private PointerEventData _pointerEventData;
    private List<RaycastResult> castResults = new List<RaycastResult>();

    public void OnDrag(PointerEventData eventData)
    {
        currentPosition = eventData.position;
        Vector2 directionRaw = originDrag - currentPosition;
        distance = directionRaw.magnitude / Screen.height;
        directionDrag = directionRaw.normalized;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        distance = 0;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        originDrag = eventData.position;
        _cameraPositionStart = _cameraTransform.position;
        currentPosition = eventData.position;
    }

    private void Awake()
    {
        Current = this;
        _pointerEventData = new PointerEventData(EventSystem.current);
    }

    private void Start()
    {
        _cameraTransform = cinemachineCamera.transform;
    }

    private void Update()
    {
        if(!CanvasesManager.IsGameStarted) return;
        if(distance != 0)
        {
            targetCameraCoordinates =  _cameraPositionStart + Vector3.forward * distance * 12 * directionDrag.y;
            var rawPosition = new Vector3(targetCameraCoordinates.x, _cameraTransform.position.y, targetCameraCoordinates.z);
            if (rawPosition.z > topBorder)
            {
                rawPosition.z = topBorder;
            }
            else if(rawPosition.z < -bottomBorder)
            {
                rawPosition.z = -bottomBorder;
            }
            _cameraTransform.position = rawPosition;
        }
    }
    public bool IsPointerOverUIObject(Vector3 clickPosition)
    {
        _pointerEventData.position = clickPosition;
        EventSystem.current.RaycastAll(_pointerEventData, castResults);
        for (int i = 0; i < castResults.Count; i++)
        {
            
            if (castResults[i].gameObject.layer != 10)
            {
                return true;
            }
        }

        return false;

    }
}
