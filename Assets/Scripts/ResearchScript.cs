﻿using System;
using TechnicalScripts;
using UnityEngine;
using UnityEngine.UI;

public class ResearchScript : MonoBehaviour
{
    public static ResearchScript Current;
    public int infantryUpgradeLvl = 1;
    public int lightTankUpgradeLvl = 1;
    public int heavyArtyUpgradeLvl = 1;
    public int heavyTankUpgradeLvl = 1;
    public int aiinfantryUpgradeLvl = 1;
    public int ailightTankUpgradeLvl = 1;
    public int aiheavyArtyUpgradeLvl = 1;
    public int aiheavyTankUpgradeLvl = 1;
    //
    [SerializeField] Button infantryButton;
    [SerializeField] Button lightTankButton;
    [SerializeField] Button heavyTankButton;
    [SerializeField] Button artyButton;

    private void Awake()
    {
        Current = this;
    }

    public void UpgradeInfantry()
    {
        infantryUpgradeLvl++;
        PlayerInfo.Current.SetResourcesText();

    }
    public void UpgradeLightTank()
    {
        lightTankUpgradeLvl++;
        PlayerInfo.Current.SetResourcesText();

    }
    public void UpgradeHeavyTank()
    {
        heavyTankUpgradeLvl++; 
        PlayerInfo.Current.SetResourcesText();

    }
    public void UpgradeArty()
    {
        heavyArtyUpgradeLvl++;
        PlayerInfo.Current.SetResourcesText();
    }

    public void ResearchForAi()
    {
        if(aiinfantryUpgradeLvl >= 5) return;
        aiinfantryUpgradeLvl++;
        aiheavyArtyUpgradeLvl++;
        aiheavyTankUpgradeLvl++;
        ailightTankUpgradeLvl++;
    }
    private void CheckButtonsForInteractable()
    {
        if (!CanvasesManager.IsGameStarted) return;
        if (infantryUpgradeLvl >= 5) infantryButton.interactable = false;
        else infantryButton.interactable = CalcMoney(SectorPanel.Current.infantryCost, true);
        if (heavyTankUpgradeLvl >= 5) heavyTankButton.interactable = false;
        else heavyTankButton.interactable = CalcMoney(SectorPanel.Current.heavyTankCost, true);
        if (lightTankUpgradeLvl >= 5) lightTankButton.interactable = false;
        else lightTankButton.interactable = CalcMoney(SectorPanel.Current.lightTankCost, true);
        if (heavyArtyUpgradeLvl >= 5) artyButton.interactable = false;
        else artyButton.interactable = CalcMoney(SectorPanel.Current.artilleryCost, true);
    }

    private void FixedUpdate()
    { 
        CheckButtonsForInteractable();
    }

    private bool CalcMoney(int price, bool isPlayer)
    {
        if (isPlayer && PlayerInfo.Current.resourcesCount >= price * 10) return true;
        if (!isPlayer && AiInfo.Current.resourcesCount >= price * 10) return true;
        return false;
    }
}
