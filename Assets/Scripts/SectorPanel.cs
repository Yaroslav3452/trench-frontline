﻿using System.Collections.Generic;
using System.Linq;
using Interfaces;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class SectorPanel : MonoBehaviour
{
    public static SectorPanel Current;
    public int resourcesUpgradeCost;
    public int bunkerUpgradeCost;
    public int trenchUpgradeCost;
    public int lightTankCost;
    public int heavyTankCost;
    public int infantryCost;
    public int artilleryCost;
    public SectorInfo chosenSectorInfo;
    public GameObject infantryButton;
    public GameObject artilleryButton;
    public GameObject tankButton;
    public GameObject bunkerButton;
    public GameObject trenchButton;
    public GameObject resourcesUpgradeButton;
    public GameObject heavyTankButton;
    public bool isClickedOnSectorPanel = false;
    public List<GameObject> chooseUnits = new List<GameObject>();
    public List<SectorInfo> movedSectors = new List<SectorInfo>();
    public bool isArtyChosen;
    [SerializeField] private GameObject infantryPrefab;
    [SerializeField] private GameObject heavyArtilleryPrefab;
    [SerializeField] private GameObject lightTankPrefab;
    [SerializeField] private GameObject heavyTankPrefab;
    [SerializeField] private GameObject attackButtonPrefab;
    [SerializeField] private GameObject movementToAllyPrefab;
    [SerializeField] private GameObject attackButtonArtyPrefab;
    //
    [SerializeField] private Slider leftUnitHPBar;
    [SerializeField] private Slider middleUnitHPBar;
    [SerializeField] private Slider rightUnitHPBar;
    //
    private List<GameObject> _createdButtons = new List<GameObject>();
    private List<GameObject> _createdButtonsForArty = new List<GameObject>();
    private Slider _sliderArtyReload;
    private int _maxFortLevel = 3;
    private int _maxTrenchLevel = 3;

    public void FortificationSectorBunker()
    {
        FortificationSectorBunker(null);
    }

    public void FortificationSectorBunker(SectorInfo sectorInfo)
    {
        if (sectorInfo == null) sectorInfo = chosenSectorInfo;
        if(Battle.Current.CheckIsSectorInAnyBattle(sectorInfo)) return;
        if (CheckForResourcesPrices(bunkerUpgradeCost, sectorInfo))
        {
            if (sectorInfo.fortificationLevel < _maxFortLevel)
            {
                sectorInfo.fortificationLevel += 1;
                sectorInfo.GetComponent<ConstructionInfo>().BuildBunker();
            }
        }

        CheckButtonsIsInteractable();
    }


    public void FortificationSectorTrench()
    {
        FortificationSectorTrench(null);
    }

    public void FortificationSectorTrench(SectorInfo sectorInfo)
    {
        if (sectorInfo == null) sectorInfo = chosenSectorInfo;
        if(Battle.Current.CheckIsSectorInAnyBattle(sectorInfo)) return;
        if (CheckForResourcesPrices(trenchUpgradeCost, sectorInfo))
        {
            if (sectorInfo.trenchLevel < _maxTrenchLevel)
            {
                sectorInfo.trenchLevel += 1;
                sectorInfo.GetComponent<ConstructionInfo>().BuildTrench();
            }
        }

        CheckButtonsIsInteractable();
    }

    public void UpgradeResources()
    {
        UpgradeResources(null);
    }

    public void UpgradeResources(SectorInfo sectorInfo)
    {
        if (sectorInfo == null) sectorInfo = chosenSectorInfo;
        if(Battle.Current.CheckIsSectorInAnyBattle(sectorInfo)) return;
        if (CheckForResourcesPrices(resourcesUpgradeCost, sectorInfo))
        {
            if (sectorInfo.resourcesLevel < 5)
            {
                sectorInfo.resourcesLevel += 1;
                sectorInfo.GetComponent<ConstructionInfo>().BuildResources();
            }
        }

        CheckButtonsIsInteractable();
    }

    public void AddInfantry()
    {
        AddInfantry(null);
    }

    public void AddInfantry(SectorInfo sectorInfo)
    {
        if (sectorInfo == null) sectorInfo = chosenSectorInfo;
        if(Battle.Current.CheckIsSectorInAnyBattle(sectorInfo)) return;
        if (CheckForResourcesPrices(infantryCost, sectorInfo))
        {
            if (sectorInfo.freeCapacity > 0)
            {
                var prefab = Instantiate(infantryPrefab,
                    sectorInfo.spawnPoints[ChooseEmptyIdx(sectorInfo)]);
                sectorInfo.cellsUnits.Add(prefab);
                sectorInfo.freeCapacity -= 1;
                if (!sectorInfo.isPlayerOwner)
                {
                    var rotation = prefab.transform.rotation;
                    rotation = new Quaternion(rotation.x, 180, rotation.z, rotation.w);
                    prefab.transform.rotation = rotation;
                }
            }
        }

        CheckButtonsIsInteractable();
    }

    public void AddHeavyArtillery()
    {
        AddHeavyArtillery(null);
    }

    public void AddHeavyArtillery(SectorInfo sectorInfo)
    {
        if (sectorInfo == null) sectorInfo = chosenSectorInfo;
        if (CheckForResourcesPrices(artilleryCost, sectorInfo))
        {
            if (sectorInfo.freeCapacity > 0)
            {
                var prefab = Instantiate(heavyArtilleryPrefab,
                    sectorInfo.spawnPoints[ChooseEmptyIdx(sectorInfo)]);
                sectorInfo.cellsUnits.Add(prefab);
                sectorInfo.freeCapacity -= 1;
                isArtyChosen = true;
                if (!sectorInfo.isPlayerOwner)
                {
                    var rotation = prefab.transform.rotation;
                    rotation = new Quaternion(rotation.x, 180, rotation.z, rotation.w);
                    prefab.transform.rotation = rotation;
                }
            }
        }
        CheckButtonsIsInteractable();
    }

    public void AddAntiTank()
    {
        AddAntiTank(null);
    }

    public void AddAntiTank(SectorInfo sectorInfo)
    {
        if (sectorInfo == null) sectorInfo = chosenSectorInfo;
        if (sectorInfo.freeCapacity > 0)
        {
            sectorInfo.antiTank += 1;
            sectorInfo.freeCapacity -= 1;
        }

        CheckButtonsIsInteractable();
    }

    public void AddHeavyTank()
    {
        AddHeavyTank(null);
    }

    public void AddHeavyTank(SectorInfo sectorInfo)
    {
        if (sectorInfo == null) sectorInfo = chosenSectorInfo;
        if(Battle.Current.CheckIsSectorInAnyBattle(sectorInfo)) return;
        if (CheckForResourcesPrices(heavyTankCost, sectorInfo))
        {
            if (sectorInfo.CalculateFreeCapacity() > 0)
            {
                var prefab = Instantiate(heavyTankPrefab,
                    sectorInfo.spawnPoints[ChooseEmptyIdx(sectorInfo)]);
                sectorInfo.cellsUnits.Add(prefab);
                sectorInfo.freeCapacity -= 1;
                if (!sectorInfo.isPlayerOwner)
                {
                    var rotation = prefab.transform.rotation;
                    rotation = new Quaternion(rotation.x, 180, rotation.z, rotation.w);
                    prefab.transform.rotation = rotation;
                }
            }
        }

        CheckButtonsIsInteractable();
    }
    public void AddLightTank()
    {
        AddLightTank(null);
    }

    public void AddLightTank(SectorInfo sectorInfo)
    {
        if (sectorInfo == null) sectorInfo = chosenSectorInfo;
        if(Battle.Current.CheckIsSectorInAnyBattle(sectorInfo)) return;
        if (CheckForResourcesPrices(lightTankCost, sectorInfo))
        {
            if (sectorInfo.CalculateFreeCapacity() > 0)
            {
                var prefab = Instantiate(lightTankPrefab,
                    sectorInfo.spawnPoints[ChooseEmptyIdx(sectorInfo)]);
                sectorInfo.cellsUnits.Add(prefab);
                sectorInfo.freeCapacity -= 1;
                if (!sectorInfo.isPlayerOwner)
                {
                    var rotation = prefab.transform.rotation;
                    rotation = new Quaternion(rotation.x, 180, rotation.z, rotation.w);
                    prefab.transform.rotation = rotation;
                }
            }
        }

        CheckButtonsIsInteractable();
    }

    public void CheckButtonsIsInteractable()
    {
        if(!chosenSectorInfo) return;
        if (chosenSectorInfo.fortificationLevel >= _maxFortLevel)
        {
            bunkerButton.GetComponent<Button>().interactable = false;
        }
        else bunkerButton.GetComponent<Button>().interactable = true;

        if (chosenSectorInfo.trenchLevel >= _maxTrenchLevel)
        {
            trenchButton.GetComponent<Button>().interactable = false;
        }
        else trenchButton.GetComponent<Button>().interactable = true;

        if (chosenSectorInfo.resourcesLevel >= 5 || chosenSectorInfo.isResources == false)
        {
            resourcesUpgradeButton.GetComponent<Button>().interactable = false;
        }
        else resourcesUpgradeButton.GetComponent<Button>().interactable = true;

        if (chosenSectorInfo.freeCapacity <= 0)
        {
            infantryButton.GetComponent<Button>().interactable = false;
            artilleryButton.GetComponent<Button>().interactable = false;
            tankButton.GetComponent<Button>().interactable = false;
            heavyTankButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            if(chosenSectorInfo.cellsUnits.Count == 0)
            {
                infantryButton.GetComponent<Button>().interactable = true;
                tankButton.GetComponent<Button>().interactable = true;
                artilleryButton.GetComponent<Button>().interactable = true;
                heavyTankButton.GetComponent<Button>().interactable = true;
                return;
            }
            if(!isArtyChosen)
            {
                infantryButton.GetComponent<Button>().interactable = true;
                tankButton.GetComponent<Button>().interactable = true;
                heavyTankButton.GetComponent<Button>().interactable = true;
                artilleryButton.GetComponent<Button>().interactable = false;
            } else if(isArtyChosen)
            {
                artilleryButton.GetComponent<Button>().interactable = true;
                infantryButton.GetComponent<Button>().interactable = false;
                tankButton.GetComponent<Button>().interactable = false;
                heavyTankButton.GetComponent<Button>().interactable = false;
            }
        }
    }

    public void SetAttackNeighbours()
    {
        if(Battle.Current.CheckIsSectorInAnyBattle(chosenSectorInfo)) return;
        foreach (var neighbour in chosenSectorInfo.neighbours)
        {
            MoveUpNeighboursInBattles(neighbour);
            if (!neighbour.isPlayerOwner)
            {
                neighbour.SelectLikeTargetForAttack();
                var attackButton = Instantiate(attackButtonPrefab, neighbour.attackButtonSpawn.transform);
                _createdButtons.Add(attackButton);
            }
            else
            {
                if (neighbour.cellsUnits.Count < 3)
                {
                    neighbour.AllySectorForMovement();
                    var attackButton = Instantiate(movementToAllyPrefab, neighbour.attackButtonSpawn.transform);
                    _createdButtons.Add(attackButton);
                }
            }

            movedSectors.Add(neighbour);
        }
    }

    public void SetAttackNeighboursForHeavyArty()
    {
        foreach (var neighbour in chosenSectorInfo.neighbours)
        {
            MoveUpNeighboursInBattles(neighbour);
            if (!neighbour.isPlayerOwner)
            {
                neighbour.SelectLikeTargetForAttack();
                var attackButton = Instantiate(attackButtonArtyPrefab, neighbour.attackButtonSpawn.transform);
                _createdButtons.Add(attackButton);
            }
            else
            {
                if (neighbour.cellsUnits.Count < 3)
                {
                    neighbour.AllySectorForMovement();
                    var attackButton = Instantiate(movementToAllyPrefab, neighbour.attackButtonSpawn.transform);
                    _createdButtons.Add(attackButton);
                }
            }

            movedSectors.Add(neighbour);
        }

        List<SectorInfo> arr = new List<SectorInfo>();
        SectorInfo sector;
        if (!(chosenSectorInfo.leftNeighbour is null) && !(chosenSectorInfo.leftNeighbour.leftNeighbour is null))
        {
            sector = chosenSectorInfo.leftNeighbour.leftNeighbour;
            MoveUpNeighboursInBattles(sector);
            arr.Add(sector);
        }

        if (!(chosenSectorInfo.rightNeighbour is null) && !(chosenSectorInfo.rightNeighbour.rightNeighbour is null))
        {
            sector = chosenSectorInfo.rightNeighbour.rightNeighbour;
            MoveUpNeighboursInBattles(sector);
            arr.Add(sector);
        }

        if (!(chosenSectorInfo.upperNeighbour is null) && !(chosenSectorInfo.upperNeighbour.upperNeighbour is null))
        {
            sector = chosenSectorInfo.upperNeighbour.upperNeighbour;
            MoveUpNeighboursInBattles(sector);
            arr.Add(sector);
        }

        if (!(chosenSectorInfo.bottomNeighbour is null) && !(chosenSectorInfo.bottomNeighbour.bottomNeighbour))
        {
            sector = chosenSectorInfo.bottomNeighbour.bottomNeighbour;
            MoveUpNeighboursInBattles(sector);
            arr.Add(sector);
        }

        foreach (var neighbour in arr)
        {
            if (!neighbour.isPlayerOwner)
            {
                neighbour.SelectLikeTargetForAttack();
                var attackButton = Instantiate(attackButtonArtyPrefab,
                    neighbour.attackButtonSpawn.transform);
                _createdButtonsForArty.Add(attackButton);
            }

            movedSectors.Add(neighbour);
        }
    }

    public void DeleteButtons()
    {
        foreach (var button in _createdButtons)
        {
            Destroy(button);
        }

        foreach (var button in _createdButtonsForArty)
        {
            Destroy(button);
        }
    }

    public void RemoveUnits()
    {
        foreach (var unit in chooseUnits.ToList())
        {
            unit.GetComponent<IMovable>().RemoveUnit();
        }
        chooseUnits.Clear();
    }
    public void RemoveUnits(SectorInfo sectorInfo)
    {
        foreach (var unit in sectorInfo.cellsUnits.ToList())
        {
            unit.GetComponent<IMovable>().RemoveUnit();
        }
    }

    public void ChangeSectorOwnerByButton()
    {
        if (chosenSectorInfo.isPlayerOwner)
        {
            chosenSectorInfo.isPlayerOwner = false;
            AiInfo.Current.aiSectors.Add(chosenSectorInfo);
        }
        else chosenSectorInfo.isPlayerOwner = true;
        chosenSectorInfo.ChangeSectorColor();
    }

    public void AttackSector([CanBeNull] SectorInfo attackerSectorInfo, SectorInfo defenderSectorInfo)
    {
        List<GameObject> units = new List<GameObject>();
        if (attackerSectorInfo == null)
        {
            attackerSectorInfo = chosenSectorInfo;
            units.AddRange(chooseUnits);
        } else
        {
            units.AddRange(attackerSectorInfo.cellsUnits);
        }
        Battle.Current.StartBattle(attackerSectorInfo, defenderSectorInfo, units);
    }

    public void AttackSectorViaArty(SectorInfo sectorInfo)
    {
        foreach (var artyUnit in chooseUnits)
        {
            artyUnit.GetComponent<HeavyArtilleryScript>()
                .AttackPosition(sectorInfo, chosenSectorInfo.cellsUnits.IndexOf(artyUnit));
        }
    }

    public void RelocateToAllySector(SectorInfo sectorInfo)
    {
        //защита от въезда в одну клетку артиллерии и других юнитов
        if (!isArtyChosen && CheckForArtyInSector(sectorInfo)) return;
        if (isArtyChosen && !CheckForArtyInSector(sectorInfo) && sectorInfo.cellsUnits.Count != 0) return;
        //
        for (int i = 0; i < chooseUnits.Count; i++)
        {
            var unit = chooseUnits[i];
            if (sectorInfo.freeCapacity > 0)
            {
                var idx = ChooseEmptyIdx(sectorInfo);
                unit.GetComponent<IMovable>().MoveToAllySector(sectorInfo, idx);
            }
        }
    }

    public void ChooseInfantry()
    {
        foreach (var unit in chosenSectorInfo.cellsUnits)
        {
            if (unit.CompareTag("Infantry") && !chooseUnits.Contains(unit)) chooseUnits.Add(unit);
            else if (unit.CompareTag("Infantry") && chooseUnits.Contains(unit)) chooseUnits.Remove(unit);
        }
    }

    public void ChooseHeavyArty()
    {
        foreach (var unit in chosenSectorInfo.cellsUnits)
        {
            if (unit.CompareTag("HeavyArtillery") && !chooseUnits.Contains(unit)) chooseUnits.Add(unit);
            else if (unit.CompareTag("HeavyArtillery") && chooseUnits.Contains(unit)) chooseUnits.Remove(unit);
        }
    }

    public void ChooseLightTank()
    {
        foreach (var unit in chosenSectorInfo.cellsUnits)
        {
            if (unit.CompareTag("LightTank") && !chooseUnits.Contains(unit)) chooseUnits.Add(unit);
            else if (unit.CompareTag("LightTank") && chooseUnits.Contains(unit)) chooseUnits.Remove(unit);
        }
    }
    public void ChooseHeavyTank()
    {
        foreach (var unit in chosenSectorInfo.cellsUnits)
        {
            if (unit.CompareTag("HeavyTank") && !chooseUnits.Contains(unit)) chooseUnits.Add(unit);
            else if (unit.CompareTag("HeavyTank") && chooseUnits.Contains(unit)) chooseUnits.Remove(unit);
        }
    }

    public int ChooseEmptyIdx(SectorInfo sectorInfo)
    {
        var idx = 0;
        for (int j = 0; j < sectorInfo.spawnPoints.Count; j++)
        {
            if (sectorInfo.spawnPoints[j].childCount == 0)
            {
                idx = j;
                break;
            }
        }

        return idx;
    }

    private bool CheckForArtyInSector(SectorInfo sectorInfo)
    {
        foreach (var unit in sectorInfo.cellsUnits)
        {
            if (unit.CompareTag("HeavyArtillery")) return true;
        }

        return false;
    }

    private bool CheckForResourcesPrices(int price, SectorInfo sectorInfo)
    {
        if (sectorInfo.isPlayerOwner)
        {
            if (PlayerInfo.Current.resourcesCount >= price)
            {
                PlayerInfo.Current.resourcesCount -= price;
                PlayerInfo.Current.SetResourcesText();
                return true;
            }
        }
        else if (!sectorInfo.isPlayerOwner)
        {
            if (AiInfo.Current.resourcesCount >= price)
            {
                AiInfo.Current.resourcesCount -= price;
                return true;
            }
        }

        return false;
    }

    private void MoveUpNeighboursInBattles(SectorInfo neighbour)
    {
        foreach (var battle in Battle.Current.BattlesList)
        {
            if (battle.AttackersSector == neighbour)
            {
                if(battle.DefendersSector.isPlayerOwner) battle.DefendersSector.AllySectorForMovement();
                else battle.DefendersSector.SelectLikeTargetForAttack();
                movedSectors.Add(battle.DefendersSector);
            } else if (battle.DefendersSector == neighbour)
            {
                if(battle.AttackersSector.isPlayerOwner) battle.AttackersSector.AllySectorForMovement();
                else battle.AttackersSector.SelectLikeTargetForAttack();
                movedSectors.Add(battle.AttackersSector);
            }
        }
    }

    private void FixedUpdate()
    {
        if(!CanvasesManager.IsGameStarted) return;
        if (chooseUnits.Count >= 0)
        {
            var maxReload = 0f;
            foreach (var unit in chooseUnits.ToList())
            {
                if(!unit) continue;
                if (unit.CompareTag("HeavyArtillery"))
                {
                    var script = unit.GetComponent<HeavyArtilleryScript>();
                    if (maxReload < script.remainingReloadTime) maxReload = script.remainingReloadTime;
                }
            }
            _sliderArtyReload.value = maxReload;
        }
        SetHpValuesForSliders();
    }

    private void SetHpValuesForSliders()
    {
        //можно оптимизировать порядок ифов
        if(!chosenSectorInfo) return;
        if (chosenSectorInfo.spawnPoints[0].childCount >= 1) leftUnitHPBar.value = chosenSectorInfo.spawnPoints[0].GetComponentInChildren<IMovable>().ReturnPercentageHpOfUnit() * 100;
        else leftUnitHPBar.value = 0;
        if(chosenSectorInfo.spawnPoints[1].childCount >= 1) middleUnitHPBar.value = chosenSectorInfo.spawnPoints[1].GetComponentInChildren<IMovable>().ReturnPercentageHpOfUnit() * 100;
        else middleUnitHPBar.value = 0;
        if(chosenSectorInfo.spawnPoints[2].childCount >= 1) rightUnitHPBar.value = chosenSectorInfo.spawnPoints[2].GetComponentInChildren<IMovable>().ReturnPercentageHpOfUnit() * 100;
        else rightUnitHPBar.value = 0;
    }

    private void Awake()
    {
        Current = this;
        _sliderArtyReload = GetComponentInChildren<Slider>();
    }
}
