﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingRange : MonoBehaviour
{
    private InfantryScript _infantryScript;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("TestTarget"))
        {
            Debug.Log("Enemy in sector");
            _infantryScript.attackTarget = other.gameObject;   
        }
    }

    private void Start()
    {
        _infantryScript = GetComponentInParent<InfantryScript>();
    }
}
