﻿using System;
using System.Collections;
using System.Collections.Generic;
using Interfaces;
using TechnicalScripts;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using Random = UnityEngine.Random;

public class HeavyArtilleryScript : MonoBehaviour, IMovable, IBattleSetter, IAttack, IAttackSectorSetter
{
    public float etalonHp;
    public float hp = 5;
    public float damage = 10;
    public float speed = 2;
    public float reloadTime = 10f;
    public float remainingReloadTime;
    public float infantryDamageMultiply = 1.5f;
    public float motorizedDamageMultiply = 1.1f;
    public float mechanizedDamageMultiply = 0.7f;
    public bool isAnimationIsAttack = false;
    public ParticleSystem gunParticlesFire;
    public ParticleSystem gunParticlesFireParticles;
    public ParticleSystem gunParticlesSmoke;
    public Transform targetPosition;
    public Battle.BattleInSector currentBattle;
    [SerializeField] private GameObject explosionPrefab;
    private Transform _transform;
    private Rigidbody _rigidbody;
    private Vector3 _deltaPosition;
    private Vector3 _movementDirection;
    private Quaternion _defaultRotation;
    private int _cellPosition;
    private SectorInfo _attackSector;
    private bool _isReadyToShoot;
    private bool _moveBackAfterShootAnimation;
    private Vector3 _recoilPosition = Vector3.zero;
    private float _randomRangeRecoil;
    [SerializeField] private AudioSource _audioSource;


    public void AttackPosition(SectorInfo sectorInfo, int cellPosition)
    {
        _cellPosition = cellPosition;
        _attackSector = sectorInfo;
        if (sectorInfo.cellsUnits.Count == 0 && GetComponentInParent<SectorInfo>().neighbours.Contains(sectorInfo))
        {
            targetPosition = sectorInfo.spawnPoints[cellPosition].transform;
        }
        else if(remainingReloadTime <= 0f && sectorInfo.cellsUnits.Count > 0)
        {
            ArtilleryShellingPosition();
        }
    }

    public void MoveToAllySector(SectorInfo sectorInfo, int cellPosition)
    {
        _cellPosition = cellPosition;
        _attackSector = sectorInfo;
        targetPosition = sectorInfo.spawnPoints[cellPosition].transform;
        _transform.LookAt(targetPosition);
        UnitReAssignForAllySector();

    }

    private void ArtilleryShellingPosition()
    {
        remainingReloadTime = reloadTime;
        ShellingAnimations();
        CalculateDamage();
    }

    private void ShellingAnimations()
    {
        _audioSource.clip = SoundManager.Current.artyShot;
        _audioSource.Play();
        gunParticlesFire.Play();
        gunParticlesSmoke.Play();
        gunParticlesFireParticles.Play();
        var position = _transform.position;
        _randomRangeRecoil = Random.Range(0.04f, 0.11f);
        _recoilPosition = new Vector3(position.x, position.y, position.z - _randomRangeRecoil);
        var enemyPosition = _attackSector.cellsUnits[Random.Range(0,_attackSector.cellsUnits.Count)].transform.position;
        var randomExplosionPoint = new Vector3(enemyPosition.x + Random.Range(-0.3f, 0.3f), enemyPosition.y, enemyPosition.z + Random.Range(-0.3f, 0.3f));
        var explosion = Instantiate(explosionPrefab, randomExplosionPoint , Quaternion.identity);
        Destroy(explosion, 2.0f);
    }

    private void CalculateDamage()
    {
        foreach (var enemy in _attackSector.cellsUnits)
        {
            if (enemy.CompareTag("Infantry"))
            {
                var infantryScript = enemy.GetComponent<InfantryScript>();
                infantryScript.hp -= damage / _attackSector.cellsUnits.Count * infantryDamageMultiply;
            }
            else if (enemy.CompareTag("HeavyArtillery"))
            {
                var artilleryScript = enemy.GetComponent<HeavyArtilleryScript>();
                artilleryScript.hp -= damage / _attackSector.cellsUnits.Count * motorizedDamageMultiply;
            }
            else if (enemy.CompareTag("HeavyArtillery"))
            {
                var tankScript = enemy.GetComponent<LightTankScript>();
                tankScript.hp -= damage / _attackSector.cellsUnits.Count * mechanizedDamageMultiply;
            }
        }
    }

    private void MovingToCell()
    {
        var pos = Vector3.MoveTowards(_transform.position, targetPosition.position, Time.deltaTime * speed / 2);
        _transform.LookAt(pos);
        _transform.position = pos;
        if (_transform.position == targetPosition.position)
        {
            if(!_attackSector.cellsUnits.Contains(gameObject))UnitReAssign();
            ResetDefaults();
        }
    }
    public void ResetDefaults()
    {
        _transform.rotation = _defaultRotation;
        isAnimationIsAttack = false;
        targetPosition = null;
    }
    public void UnitReAssign()
    {
        if(_attackSector.cellsUnits.Count > _cellPosition) _attackSector.cellsUnits[_cellPosition] = gameObject;
        else _attackSector.cellsUnits.Add(gameObject);
        var previousSector = GetComponentInParent<SectorInfo>();
        previousSector.cellsUnits.Remove(gameObject);
        previousSector.CalculateFreeCapacity();
        GetComponentInParent<SectorInfo>().CalculateFreeCapacity();
        _attackSector.isPlayerOwner = true;
        _attackSector.ChangeSectorColor();
        transform.parent = _attackSector.spawnPoints[_cellPosition].transform;
    }

    public void UnitReAssignForAllySector()
    {
        if(!_attackSector.cellsUnits.Contains(gameObject)) _attackSector.cellsUnits.Add(gameObject);
        var previousSector = GetComponentInParent<SectorInfo>();
        if(_attackSector != previousSector)
        {
            previousSector.cellsUnits.Remove(gameObject);
            previousSector.CalculateFreeCapacity();
            transform.parent = _attackSector.spawnPoints[_cellPosition].transform;
            GetComponentInParent<SectorInfo>().CalculateFreeCapacity();
        }
    }
    private void Start()
    {
        _transform = transform;
        _rigidbody = GetComponent<Rigidbody>();
        _defaultRotation = _transform.rotation;
        etalonHp = hp;
    }

    private void Update()
    {
        if(!CanvasesManager.IsGameStarted) return;
        if (targetPosition != null) MovingToCell();
        if (_recoilPosition != Vector3.zero)
        {
            var position = transform.parent.transform.position;
            _transform.position = Vector3.Lerp(_transform.position, new Vector3(position.x,position.y, _recoilPosition.z), 0.02f);
            if(Mathf.Abs(_recoilPosition.z - _transform.position.z) < 0.01 && !_moveBackAfterShootAnimation)
            {
                _recoilPosition = transform.parent.transform.position;
                _moveBackAfterShootAnimation = true;
            } else if(Mathf.Abs(_recoilPosition.z - _transform.position.z) < 0.01 && _moveBackAfterShootAnimation)
            {
                _moveBackAfterShootAnimation = false;
                _recoilPosition = Vector3.zero;
                ResetDefaults();
            }
        }
    }
    private void FixedUpdate()
    {
        if(!CanvasesManager.IsGameStarted) return;
        remainingReloadTime -= Time.fixedDeltaTime;
        HpRegeneration();
    }
    public void TrooperIsDead()
    {
        var explosion = Instantiate(explosionPrefab, _transform.position, Quaternion.identity);
        Destroy(explosion, 5f);
        Debug.Log("arty is destroyed");
        if (currentBattle.AttackSide.Contains(gameObject))
        {
            currentBattle.AttackSide.Remove(gameObject);
        } else
        if (currentBattle.DefendSide.Contains(gameObject))
        {
            currentBattle.DefendSide.Remove(gameObject);
        }
        GetComponentInParent<SectorInfo>().cellsUnits.Remove(gameObject);
        GetComponentInParent<SectorInfo>().artillery--;
        GetComponentInParent<SectorInfo>().CalculateFreeCapacity();
        Destroy(gameObject);
    }
    public void SetCurrentBattle(Battle.BattleInSector battleInSector)
    {
        currentBattle = battleInSector;
    }
    public void SetAttackSector(SectorInfo sectorInfo)
    {
        _attackSector = sectorInfo;
    }
    public void SetPositionToSpawnPoint()
    {
        var sectorInfo = GetComponentInParent<SectorInfo>();
        targetPosition = sectorInfo.spawnPoints[sectorInfo.cellsUnits.IndexOf(gameObject)];
    }
    public void SetAttackSectorToParentSector()
    {
        _attackSector = GetComponentInParent<SectorInfo>();
    }
    public void RemoveUnit()
    {
        var componentInParent = GetComponentInParent<SectorInfo>();
        componentInParent.cellsUnits.Remove(gameObject);
        componentInParent.artillery--;
        componentInParent.CalculateFreeCapacity();
        if (componentInParent.isPlayerOwner)
            PlayerInfo.Current.resourcesCount += SectorPanel.Current.artilleryCost * 0.5f;
        else AiInfo.Current.resourcesCount += SectorPanel.Current.artilleryCost;
        Destroy(gameObject);
    }
    public Transform ReturnTargetPosition()
    {
        return targetPosition;
    }

    public float ReturnPercentageHpOfUnit()
    {
        return hp / etalonHp;
    }

    public void HpRegeneration()
    {
        if(Battle.Current.CheckIsSectorInAnyBattle(GetComponentInParent<SectorInfo>())) return;
        if (hp < etalonHp)
        {
            hp += hp * Settings.Current.hpRegenerationPercent;
            if (hp > etalonHp) hp = etalonHp;
        }
    }
}
