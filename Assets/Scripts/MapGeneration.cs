﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MapGeneration : MonoBehaviour
{
    public static MapGeneration Current;
    public int[,] gameDeck;
    [SerializeField] private GameObject groundParent;
    [SerializeField] private GameObject groundGroup;
    [SerializeField] private GameObject resourcesGroup;
    private int _lengthOfDeck;
    
    private void Awake()
    {
        Current = this;
        _lengthOfDeck = Random.Range(12, 20);
        if (_lengthOfDeck % 2 != 0) _lengthOfDeck--;
        gameDeck = new int[_lengthOfDeck, 5];
        Generation();
        PrintArr();
        GenerateGameObjects();
    }

    private void GenerateGameObjects()
    {
        for (int i = 0; i < _lengthOfDeck; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                GameObject kek = null;
                if(gameDeck[i,j] == 1) continue;
                if (gameDeck[i,j] == 5)
                {
                     kek = Instantiate(resourcesGroup, new Vector3((1.55f * j - 1.55f) - 1.55f, 0, 1.55f * i -10f), Quaternion.identity, groundParent.transform);
                     kek.GetComponent<SectorInfo>().isResources = true;
                }
                if (gameDeck[i, j] == 0)
                {
                    kek = Instantiate(groundGroup, new Vector3((1.55f * j - 1.55f) - 1.55f, 0, 1.55f * i -10f), Quaternion.identity, groundParent.transform);
                }

                if (i > _lengthOfDeck / 2 - 1) kek.GetComponent<SectorInfo>().isPlayerOwner = false;
                else kek.GetComponent<SectorInfo>().isPlayerOwner = true;
            }
        }
    }
    private void Generation()
    {
        for (int i = 0; i < _lengthOfDeck; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                gameDeck[i, j] = CalcCellType();
            }
        }
        CheckForMistakes();
        CheckIsResourcesAreFair();
    }

    private void CheckForMistakes()
    {
        for (int i = 0; i < _lengthOfDeck; i++)
        {
            if (gameDeck[i, 0] == 1 && gameDeck[i, 1] == 1 && gameDeck[i, 2] == 1)
            {
                gameDeck[i, Random.Range(0, 1)] = 0;
                gameDeck[i, Random.Range(0, 2)] = 0;
            }

            if (gameDeck[i, 2] == 1 && gameDeck[i, 3] == 1 && gameDeck[i, 4] == 1)
            {
                gameDeck[i, Random.Range(0, 3)] = 0;
                gameDeck[i, Random.Range(0, 4)] = 0;
            }
        }
    }
    private void PrintArr()
    {
        Debug.Log("---Ai part---");
        for (int i = 0; i < _lengthOfDeck / 2; i++)
        {
            Debug.Log(gameDeck[i, 0] + " | " + gameDeck[i, 1] + " | " + gameDeck[i, 2] + " | " + gameDeck[i, 3] + " | " + gameDeck[i, 4]);
        } 
        Debug.Log("---Player part---");
        for (int i = _lengthOfDeck / 2; i < _lengthOfDeck; i++)
        {
            Debug.Log(gameDeck[i, 0] + " | " + gameDeck[i, 1] + " | " + gameDeck[i, 2] + " | " + gameDeck[i, 3] + " | " + gameDeck[i, 4]);
        } 
    }
    
    private void CheckIsResourcesAreFair()
    {
        int countOfResources = Random.Range(1, (int)(_lengthOfDeck * 3 * 0.05f));
        if (countOfResources % 2 != 0) countOfResources++;
        for (int z = 0; z < countOfResources; z++)
        {
            int rLength = Random.Range(0, _lengthOfDeck / 2);
            int rWidth = Random.Range(0, 5);
            while(gameDeck[rLength, rWidth] != 0)
            {
                rLength = Random.Range(0, _lengthOfDeck / 2);
                rWidth = Random.Range(0, 5);
            }
            gameDeck[rLength, rWidth] = 5;
            rLength = Random.Range(_lengthOfDeck / 2, _lengthOfDeck - 1);
            rWidth = Random.Range(0, 5);
            while(gameDeck[rLength, rWidth] != 0)
            {
                rLength = Random.Range(_lengthOfDeck / 2, _lengthOfDeck - 1);
                rWidth = Random.Range(0, 5);
            }
            gameDeck[rLength, rWidth] = 5;
        }
    }

    private int CalcCellType()
    {
        var chance = Random.Range(0, 25);
        if (chance == 5) return 1;
        return 0;
    }
}
