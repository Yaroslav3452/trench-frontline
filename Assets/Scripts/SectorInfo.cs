﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SectorInfo : MonoBehaviour
{
    public static SectorInfo Current;
    public bool isPlayerOwner;
    public bool isResources;
    public int resourcesLevel;
    public int fortificationLevel = 0;
    public int trenchLevel = 0;
    public float fortificationHp;
    public int freeCapacity = 5;
    public int infantry = 0;
    public int artillery;
    public int antiTank = 0;
    public int lightTank = 0;
    public int heavyTank = 0;
    public List<GameObject> cellsUnits = new List<GameObject>();
    public List<SectorInfo> neighbours = new List<SectorInfo>();
    public SectorInfo leftNeighbour;
    public SectorInfo rightNeighbour;
    public SectorInfo bottomNeighbour;
    public SectorInfo upperNeighbour;
    public List<Transform> spawnPoints = new List<Transform>();
    public GameObject attackButtonSpawn;
    public List<BlockInfo> blockInfos = new List<BlockInfo>();
    [SerializeField] private int targetBunkerLvl = 0;
    [SerializeField] private int targetTrenchLvl = 0;
    [SerializeField] private int targetResourceLvl = 0;
    [SerializeField] private List<GameObject> frontBlocks = new List<GameObject>();
    private Transform _transform;
    private Vector3 _defaultPosition;
    private Vector3 _newPosition;
    public void MoveUpWhenChosen()
    {
        SectorPanel.Current.chosenSectorInfo = this;
        var position = _transform.position;
        _newPosition = new Vector3(position.x, 0.2f, position.z);
        CanvasesManager.Current.sectorInfoPanel.SetActive(true);
        SectorPanel.Current.CheckButtonsIsInteractable();
        foreach (var unit in cellsUnits)
        {
            if (unit.CompareTag("HeavyArtillery"))
            {
                SectorPanel.Current.isArtyChosen = true;
                SectorPanel.Current.SetAttackNeighboursForHeavyArty();
                break;
            }
        }
        if(!SectorPanel.Current.isArtyChosen) SectorPanel.Current.SetAttackNeighbours();
        SectorPanel.Current.chooseUnits.AddRange(cellsUnits);
    }
    public void SelectLikeTargetForAttack()
    {
        var position = _transform.position;
        _newPosition = new Vector3(position.x, 0.2f, position.z);
        SetTargetColorsForBlocks(Color.red);
    }

    public void TargetAttackMoveBackSector()
    {
        MoveBlockBack(); 
        SetTargetColorsToDefaultColorsForBlocks();
    }

    public void AllySectorForMovement()
    {
        var position = _transform.position;
        _newPosition = new Vector3(position.x, 0.2f, position.z);
        SetTargetColorsForBlocks(Color.gray);
    }

    public void MoveBlockBack()
    {
        _newPosition = _defaultPosition;
        SetTargetColorsToDefaultColorsForBlocks();

    }

    public void MoveBackWhenNotChosen()
    {
        SectorPanel.Current.DeleteButtons();
        SectorPanel.Current.chosenSectorInfo = null;
        MoveBlockBack();
    }

    public void ChangeSectorColor()
    {
        if (isPlayerOwner && AiInfo.Current.resourceSilos.Contains(this))
        {
            AiInfo.Current.resourceSilos.Remove(this);
            PlayerInfo.Current.resourceSilos.Add(this);
        }
        else if (!isPlayerOwner && PlayerInfo.Current.resourceSilos.Contains(this))
        {
            PlayerInfo.Current.resourceSilos.Remove(this);
            AiInfo.Current.resourceSilos.Add(this);
        }
        ChangeDefaultColorsForBlocks();
    }

    public int CalculateFreeCapacity()
    {
        freeCapacity = 3 - cellsUnits.Count;
        return freeCapacity;
    }

    private void OnMouseDown()
    {
        if (!isPlayerOwner) return;
        if (TouchPad.Current.IsPointerOverUIObject(Input.mousePosition)) return;
        // if(Battle.Current)
        if (SectorPanel.Current.chosenSectorInfo != null) CanvasesManager.Current.CloseSectorInfo();
        MoveUpWhenChosen();
    }

    private void Awake()
    {
        Current = this;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(gameObject.name + " collide with " + other.gameObject.name);
        if(!neighbours.Contains(other.gameObject.GetComponent<SectorInfo>())) neighbours.Add(other.gameObject.GetComponent<SectorInfo>());
    }

    private void Update()
    {
        if(!CanvasesManager.IsGameStarted) return;
        if(Math.Abs(_newPosition.y - _transform.position.y) > 0.01f)
        {
            _transform.position = Vector3.Lerp(_transform.position, _newPosition, 0.1f);
        }

        foreach (var block in blockInfos)
        {
            if (block.meshRenderer.material.color != block.targetColor)
            {
                block.meshRenderer.material.color = Vector4.Lerp(block.meshRenderer.material.color, block.targetColor, 0.1f);
            }
        }
    }

    private void Start()
    {
        if (isPlayerOwner && isResources) PlayerInfo.Current.resourceSilos.Add(this);
        else if(!isPlayerOwner && isResources) AiInfo.Current.resourceSilos.Add(this);
        _transform = transform;
        _defaultPosition = _transform.position;
        blockInfos.AddRange(GetComponentsInChildren<BlockInfo>());
        foreach (var block in blockInfos)
        {
            if (!isPlayerOwner)
            {
                block.meshRenderer.material.color = Color.gray;
                block.currentDefaultColor = Color.gray;
                block.targetColor = Color.gray;
            }
        }
        GetSpawnPoints();
        CheckForNeighboursOnStart();
        SpawnUnitsOnStart();
        BuildBunkersOnStart();
        BuildResourcesOnStart();
    }

    private void SpawnUnitsOnStart()
    {
        for (int i = 0; i < infantry; i++)
        {
            SectorPanel.Current.AddInfantry(this);
        }

        for (int i = 0; i < artillery; i++)
        {
            SectorPanel.Current.AddHeavyArtillery(this);
        }
        for (int i = 0; i < lightTank; i++)
        {
            SectorPanel.Current.AddLightTank(this);
        }

        for (int i = 0; i < heavyTank; i++)
        {
            SectorPanel.Current.AddHeavyTank(this);
        }
    }

    private void BuildBunkersOnStart()
    {
        for (int i = 0; i < targetBunkerLvl; i++)
        {
            SectorPanel.Current.FortificationSectorBunker(this);
        }

        for (int i = 0; i < targetTrenchLvl; i++)
        {
            SectorPanel.Current.FortificationSectorTrench(this);
        }
    }

    private void BuildResourcesOnStart()
    {
        for (int i = 0; i < targetResourceLvl; i++)
        {
            SectorPanel.Current.UpgradeResources(this);
        }
    }

    private void CheckForNeighboursOnStart()
    {
        if (Physics.Raycast(_transform.position, Vector3.back, out var hitBack, 1f))
        {
            if (hitBack.collider != null)
            {
                if (hitBack.collider.gameObject.GetComponent<SectorInfo>())
                {
                    var sector = hitBack.collider.gameObject.GetComponent<SectorInfo>();
                    neighbours.Add(sector);
                    bottomNeighbour = sector;
                }
            }
        }

        if (Physics.Raycast(_transform.position, Vector3.forward, out var hitForward, 1f))
        {
            if (hitForward.collider != null)
            {
                if (hitForward.collider.gameObject.GetComponent<SectorInfo>())
                {
                    var sector = hitForward.collider.gameObject.GetComponent<SectorInfo>();
                    neighbours.Add(sector);
                    upperNeighbour = sector;
                }
            }
        }

        if (Physics.Raycast(_transform.position, Vector3.left, out var hitLeft, 1f))
        {
            if (hitLeft.collider != null)
            {
                if (hitLeft.collider.gameObject.GetComponent<SectorInfo>())
                {
                    var sector = hitLeft.collider.gameObject.GetComponent<SectorInfo>();
                    neighbours.Add(sector);
                    leftNeighbour = sector;
                }
            }
        }

        if (Physics.Raycast(_transform.position, Vector3.right, out var hitRight, 1f)){
            if (hitRight.collider != null)
            {
                if (hitRight.collider.gameObject.GetComponent<SectorInfo>())
                {
                    var sector = hitRight.collider.gameObject.GetComponent<SectorInfo>();
                    neighbours.Add(sector);
                    rightNeighbour = sector;
                }
            }
        }
    }

    private void SetTargetColorsForBlocks(Color color)
    {
        foreach (var blockInfo in blockInfos)
        {
            blockInfo.targetColor = color;
        }
    }

    private void SetTargetColorsToDefaultColorsForBlocks()
    {
        foreach (var blockInfo in blockInfos)
        {
            blockInfo.targetColor = blockInfo.currentDefaultColor;
        }
    }

    private void ChangeDefaultColorsForBlocks()
    {
        foreach (var blockInfo in blockInfos)
        {
            if(!isPlayerOwner) blockInfo.currentDefaultColor = blockInfo.defaultEnemyColor;
            else blockInfo.currentDefaultColor = blockInfo.defaultColor;
            blockInfo.targetColor = blockInfo.currentDefaultColor;
        }
    }

    private void GetSpawnPoints()
    {
        foreach (var block in frontBlocks)
        {
            var script = block.GetComponentInChildren<BlockInfo>();
            spawnPoints.Add(script.unitSpawnPoint);
        }
    }
}