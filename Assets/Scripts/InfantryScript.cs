﻿using Interfaces;
using TechnicalScripts;
using UnityEngine;

public class InfantryScript : MonoBehaviour, IMovable, IBattleSetter, IAttack, IAttackSectorSetter, IAIinformationSet
{
    public float etalonHp;
    public float hp = 10;
    public float speed = 2;
    public float damage = 10;
    public float attackRange = 0.6f;
    public float infantryDamageMultiply = 1f;
    public float motorizedDamageMultiply = 0.7f;
    public float mechanizedDamageMultiply = 0.2f;
    public float infantrySpeedForAnimator = 0;
    public bool isAnimationIsAttack = false;
    public GameObject attackTarget;
    public Transform targetPosition;
    public ParticleSystem gunParticlesFire;
    public ParticleSystem gunParticlesFireParticles;
    public ParticleSystem gunParticlesSmoke;
    public Battle.BattleInSector currentBattle;
    private Transform _transform;
    private Rigidbody _rigidbody;
    private Animator _animator;
    private Vector3 _deltaPosition;
    private Vector3 _movementDirection;
    private Quaternion _defaultRotation;
    private int _cellPosition;
    private SectorInfo _attackSector;
    private bool _isReadyToShoot;
    [SerializeField] private AudioSource _audioSource;
    private static readonly int Movement = Animator.StringToHash("Movement");
    private static readonly int Attack = Animator.StringToHash("Attack");
    private static readonly int Guard = Animator.StringToHash("Guard");

    public void ShootingInTarget()
    {
        if (!attackTarget && currentBattle.AttackSide.Contains(gameObject))
            Battle.Current.ChoosePriorityEnemyForInfantry(this, currentBattle);
        else if (!attackTarget && currentBattle.DefendSide.Contains(gameObject))
            Battle.Current.ChoosePriorityEnemyForInfantry(this, currentBattle);
        if (attackTarget)
        {
            _transform.LookAt(attackTarget.transform);
            ShellingAnimations();
            var distance = (_transform.position - attackTarget.transform.position).magnitude;
            if (distance <= attackRange ||
                (currentBattle.DefendSide.Contains(gameObject) && distance <= attackRange * 1.25f))
            {
                isAnimationIsAttack = true;
                var fortificationDamageMultiply = 1f;
                if (currentBattle.AttackSide.Contains(gameObject))
                    fortificationDamageMultiply =
                        1 - 0.1f * _attackSector.fortificationLevel - 0.05f * _attackSector.trenchLevel;
                if (attackTarget.CompareTag("Infantry"))
                {
                    var script = attackTarget.GetComponent<InfantryScript>();
                    script.hp -= damage * infantryDamageMultiply * fortificationDamageMultiply;
                    if (script.hp <= 0) script.TrooperIsDead();
                }
                else if (attackTarget.CompareTag("LightTank"))
                {
                    var script = attackTarget.GetComponent<LightTankScript>();
                    script.hp -= damage * mechanizedDamageMultiply * fortificationDamageMultiply;
                    if (script.hp <= 0) script.TrooperIsDead();
                }
                else if (attackTarget.CompareTag("HeavyTank"))
                {
                    var script = attackTarget.GetComponent<HeavyTankScript>();
                    script.hp -= damage * mechanizedDamageMultiply * fortificationDamageMultiply;
                    if (script.hp <= 0) script.TrooperIsDead();
                }
            }
        }

        if (attackTarget == null ||
            targetPosition == null && !currentBattle.DefendSide.Contains(gameObject))
        {
            Battle.Current.ChoosePriorityEnemyForInfantry(this, currentBattle);
        }
    }

    public void AttackPosition(SectorInfo sectorInfo, int cellPosition)
    {
        _cellPosition = cellPosition;
        _attackSector = sectorInfo;
        _transform.LookAt(targetPosition);
        _animator.ResetTrigger(Guard);
    }

    public void MoveToAllySector(SectorInfo sectorInfo, int cellPosition)
    {
        _cellPosition = cellPosition;
        _attackSector = sectorInfo;
        targetPosition = sectorInfo.spawnPoints[cellPosition].transform;
        _transform.LookAt(targetPosition);
        _animator.ResetTrigger(Guard);
        UnitReAssignForAllySector();

    }

    private void ShellingAnimations()
    {
        _audioSource.clip = SoundManager.Current.infantryShot;
        _audioSource.Play();
        gunParticlesFire.Play();
        gunParticlesSmoke.Play();
        gunParticlesFireParticles.Play();
    }

    private void UnitReAssign()
    {
        _attackSector.cellsUnits.Add(gameObject);
        var previousSector = GetComponentInParent<SectorInfo>();
        previousSector.cellsUnits.Remove(gameObject);
        previousSector.CalculateFreeCapacity();
        previousSector.infantry--;
        _attackSector.infantry++;
        if (GetComponentInParent<SectorInfo>().isPlayerOwner) _attackSector.isPlayerOwner = true;
        else _attackSector.isPlayerOwner = false;
        transform.parent = targetPosition;
        _attackSector.ChangeSectorColor();
        GetComponentInParent<SectorInfo>().CalculateFreeCapacity();
        if (currentBattle.AttackSide.Contains(gameObject))
        {
            currentBattle.AttackSide.Remove(gameObject);
            Battle.Current.CheckIsNeedToDestroyBattle(currentBattle);
        }
        AiInfo.Current.GetAISectors();
    }

    private void UnitReAssignForAllySector()
    {
        if (!_attackSector.cellsUnits.Contains(gameObject)) _attackSector.cellsUnits.Add(gameObject);
        var previousSector = GetComponentInParent<SectorInfo>();
        previousSector.cellsUnits.Remove(gameObject);
        previousSector.CalculateFreeCapacity();
        previousSector.infantry--;
        _attackSector.infantry++;
        transform.parent = _attackSector.spawnPoints[_cellPosition].transform;
        GetComponentInParent<SectorInfo>().CalculateFreeCapacity();

    }

    public void ResetDefaults()
    {
        _transform.rotation = _defaultRotation;
        infantrySpeedForAnimator = 0;
        _animator.SetTrigger(Guard);
        isAnimationIsAttack = false;
        targetPosition = null;
        attackTarget = null;
    }

    private void MovingToCell()
    {
        var pos = Vector3.MoveTowards(_transform.position, targetPosition.position, Time.deltaTime * speed / 2);
        _transform.LookAt(pos);
        _transform.position = pos;
        if (_transform.position == targetPosition.position)
        {
            if (!_attackSector.cellsUnits.Contains(gameObject)) UnitReAssign();
            ResetDefaults();
        }
    }

    public void TrooperIsDead()
    {
        Debug.Log("infantry is destroyed");
        if (currentBattle.AttackSide.Contains(gameObject))
        {
            currentBattle.AttackSide.Remove(gameObject);
        }
        else if (currentBattle.DefendSide.Contains(gameObject))
        {
            currentBattle.DefendSide.Remove(gameObject);
        }

        GetComponentInParent<SectorInfo>().cellsUnits.Remove(gameObject);
        GetComponentInParent<SectorInfo>().infantry--;
        GetComponentInParent<SectorInfo>().CalculateFreeCapacity();
        Destroy(gameObject);
    }

    private void Start()
    {
        _transform = transform;
        _rigidbody = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
        _defaultRotation = _transform.rotation;
        etalonHp = hp;
    }

    private void Update()
    {
        if(!CanvasesManager.IsGameStarted) return;
        _animator.SetFloat(Movement, infantrySpeedForAnimator);
        if (isAnimationIsAttack) _animator.SetTrigger(Attack);
        else _animator.ResetTrigger(Attack);
        if (targetPosition != null)
        {
            if (attackTarget)
            {
                if ((_transform.position - attackTarget.transform.position).magnitude > attackRange)
                {
                    infantrySpeedForAnimator = 1;
                    MovingToCell();
                }
                else
                {
                    infantrySpeedForAnimator = 0;
                }
            }
            else
            {
                infantrySpeedForAnimator = 1;
                MovingToCell();
            }
        }

        if (hp <= 0) TrooperIsDead();
    }
    private void FixedUpdate()
    {
        if(!CanvasesManager.IsGameStarted) return;
        HpRegeneration();
    }

    public void SetCurrentBattle(Battle.BattleInSector battleInSector)
    {
        currentBattle = battleInSector;
    }

    public void SetAttackSector(SectorInfo sectorInfo)
    {
        _attackSector = sectorInfo;
    }

    public void SetPositionToSpawnPoint()
    {
        var sectorInfo = GetComponentInParent<SectorInfo>();
        targetPosition = sectorInfo.spawnPoints[sectorInfo.cellsUnits.IndexOf(gameObject)];
    }

    public void SetAttackSectorToParentSector()
    {
        _attackSector = GetComponentInParent<SectorInfo>();
    }

    public float ReturnThreat()
    {
        if(GetComponentInParent<SectorInfo>().isPlayerOwner) return 4f * (1 + 0.1f * ResearchScript.Current.infantryUpgradeLvl);
        return 4f * (1 + 10f / ResearchScript.Current.aiinfantryUpgradeLvl);
    }

    public int ReturnTypeOfUnit()
    {
        return 1;
    }

    public void RemoveUnit()
    {
        var componentInParent = GetComponentInParent<SectorInfo>();
        componentInParent.cellsUnits.Remove(gameObject);
        componentInParent.infantry--;
        componentInParent.CalculateFreeCapacity();
        if (componentInParent.isPlayerOwner)
            PlayerInfo.Current.resourcesCount += SectorPanel.Current.infantryCost * 0.5f;
        else AiInfo.Current.resourcesCount += SectorPanel.Current.infantryCost;
        Destroy(gameObject);
    }

    public Transform ReturnTargetPosition()
    {
        return targetPosition;
    }

    public float ReturnPercentageHpOfUnit()
    {
        return hp / etalonHp;
    }
    public void HpRegeneration()
    {
        if(Battle.Current.CheckIsSectorInAnyBattle(GetComponentInParent<SectorInfo>())) return;
        if (hp < etalonHp)
        {
            hp += hp * Settings.Current.hpRegenerationPercent;
            if (hp > etalonHp) hp = etalonHp;
        }
    }
}