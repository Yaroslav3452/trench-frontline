﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour
{
   public static PlayerInfo Current;
   public List<SectorInfo> resourceSilos = new List<SectorInfo>();
   public float resourcesCount = 0;
   public float resourcesIncomeGlobal;
   public int resourceIncome;
   public TextMeshProUGUI resourcesCountText;
   private float _timer;

   private void Awake()
   {
      Current = this;
   }

   private void FixedUpdate()
   {
      if(!CanvasesManager.IsGameStarted) return;
      _timer += Time.fixedDeltaTime;
      if (_timer >= 5f)
      {
         AddResources();
         _timer = 0;
         SetResourcesText();
      }
   }

   public void SetResourcesText()
   {
      resourcesCountText.text = "Resources: " + resourcesCount;
   }

   private void AddResources()
   {
      var income = 0f;
      foreach (var resourceSilo in resourceSilos)
      {
         income += resourceSilo.resourcesLevel * resourceIncome;
      }
      resourcesCount += income;
      resourcesIncomeGlobal = income;
   }
}
