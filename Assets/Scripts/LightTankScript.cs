﻿using System;
using System.Collections;
using System.Collections.Generic;
using Interfaces;
using TechnicalScripts;
using UnityEngine;
using Random = UnityEngine.Random;

public class LightTankScript : MonoBehaviour, IMovable, IBattleSetter, IAttack, IAttackSectorSetter, IAIinformationSet
{
    public float etalonHp;
    public float hp = 5;
    public float damage = 10;
    public float speed = 2;
    public float attackRange = 0.7f;
    public float infantryDamageMultiply = 1.5f;
    public float motorizedDamageMultiply = 1.1f;
    public float mechanizedDamageMultiply = 0.7f;
    public bool isAnimationIsAttack = false;
    public GameObject attackTarget;
    public ParticleSystem gunParticlesFire;
    public ParticleSystem gunParticlesFireParticles;
    public ParticleSystem gunParticlesSmoke;
    public ParticleSystem engineSmoke;
    public Transform targetPosition;
    public Battle.BattleInSector currentBattle;
    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] private GameObject turret;
    private Transform _transform;
    private Rigidbody _rigidbody;
    private Vector3 _deltaPosition;
    private Vector3 _movementDirection;
    private Quaternion _defaultRotation;
    private Quaternion _targetRotation;
    private Quaternion _defaultRotationForTurret;
    private int _cellPosition;
    private SectorInfo _attackSector;
    private bool _isReadyToShoot;
    [SerializeField] private AudioSource _audioSource;


    public void ShootingInTarget()
    {  
        if(!attackTarget && currentBattle.AttackSide.Contains(gameObject)) Battle.Current.ChoosePriorityEnemyForLightTank(this, currentBattle);
        else if(!attackTarget && currentBattle.DefendSide.Contains(gameObject)) Battle.Current.ChoosePriorityEnemyForLightTankDefender(this, currentBattle);
        if (attackTarget)
        {
            var position = attackTarget.transform.position;
            turret.transform.LookAt(position);
            var distance = (_transform.position - position).magnitude;
            if (distance <= attackRange ||
                (currentBattle.DefendSide.Contains(gameObject) && distance <= attackRange * 1.25f))
            {
                ShellingAnimations();
                isAnimationIsAttack = true;
                var fortificationDamageMultiply = 1f;
                float damageWithModifiers = 0;
                if(GetComponentInParent<SectorInfo>().isPlayerOwner) damageWithModifiers = damage * (1 + 0.1f * ResearchScript.Current.lightTankUpgradeLvl);
                else damageWithModifiers = damage * (1 + 0.1f * ResearchScript.Current.ailightTankUpgradeLvl);
                if(currentBattle.AttackSide.Contains(gameObject)) fortificationDamageMultiply= 1 - 0.1f * _attackSector.fortificationLevel - 0.05f * _attackSector.trenchLevel;
                if (attackTarget.CompareTag("Infantry"))
                {
                    var script = attackTarget.GetComponent<InfantryScript>();
                    script.hp -= damageWithModifiers * infantryDamageMultiply * fortificationDamageMultiply;
                    if (script.hp <= 0) script.TrooperIsDead();
                }
                else if (attackTarget.CompareTag("LightTank"))
                {
                    var script = attackTarget.GetComponent<LightTankScript>();
                    script.hp -= damageWithModifiers * mechanizedDamageMultiply  * fortificationDamageMultiply;
                    if (script.hp <= 0) script.TrooperIsDead();
                } 
                else if (attackTarget.CompareTag("HeavyTank"))
                {
                    var script = attackTarget.GetComponent<HeavyTankScript>();
                    script.hp -= damageWithModifiers * mechanizedDamageMultiply  * fortificationDamageMultiply;
                    if (script.hp <= 0) script.TrooperIsDead();
                }
            }
        }
        
        if (attackTarget == null || targetPosition == null && !currentBattle.DefendSide.Contains(gameObject))
        {
            turret.transform.rotation = _defaultRotationForTurret;
            Battle.Current.ChoosePriorityEnemyForLightTank(this, currentBattle);
        }
    }
    public void AttackPosition(SectorInfo sectorInfo, int cellPosition)
    {
        _cellPosition = cellPosition;
        _attackSector = sectorInfo;
    }

    public void MoveToAllySector(SectorInfo sectorInfo, int cellPosition)
    {
        _cellPosition = cellPosition;
        _attackSector = sectorInfo;
        _targetRotation = _defaultRotation;
        targetPosition = sectorInfo.spawnPoints[cellPosition].transform;
        _transform.LookAt(targetPosition);
        UnitReAssignForAllySector();
       
    }
    
    public void TrooperIsDead()
    {
        var explosion = Instantiate(explosionPrefab, _transform.position, Quaternion.identity);
        Destroy(explosion, 5f);
        if (currentBattle.AttackSide.Contains(gameObject))
        {
            currentBattle.AttackSide.Remove(gameObject);
        } else
        if (currentBattle.DefendSide.Contains(gameObject))
        {
            currentBattle.DefendSide.Remove(gameObject);
        }
        GetComponentInParent<SectorInfo>().cellsUnits.Remove(gameObject);
        GetComponentInParent<SectorInfo>().lightTank--;
        GetComponentInParent<SectorInfo>().CalculateFreeCapacity();
        Destroy(gameObject);
    }

    public void RemoveUnit()
    {
        var componentInParent = GetComponentInParent<SectorInfo>();
        componentInParent.cellsUnits.Remove(gameObject);
        componentInParent.lightTank--;
        componentInParent.CalculateFreeCapacity();
        if (componentInParent.isPlayerOwner)
            PlayerInfo.Current.resourcesCount += SectorPanel.Current.lightTankCost * 0.5f;
        else AiInfo.Current.resourcesCount += SectorPanel.Current.lightTankCost;
        Destroy(gameObject);
    }

    private void ShellingAnimations()
    {
        _audioSource.clip = SoundManager.Current.lightTankShot;
        _audioSource.Play();
        gunParticlesFire.Play();
        gunParticlesSmoke.Play();
        gunParticlesFireParticles.Play();
    }

    private void MovingToCell()
    {
        if(_audioSource.clip != SoundManager.Current.mechanizedMoving || !_audioSource.isPlaying)
        {
            _audioSource.clip = SoundManager.Current.mechanizedMoving;
            _audioSource.loop = true;
            _audioSource.Play();
        }
        var pos = Vector3.MoveTowards(_transform.position, targetPosition.position, Time.deltaTime * speed / 2);
        _transform.LookAt(pos);
        _transform.position = pos;
        engineSmoke.Play();
        if (_transform.position == targetPosition.position)
        {
            _audioSource.loop = false;
            _audioSource.Stop();
            engineSmoke.Stop();
            if(!targetPosition.GetComponentInParent<SectorInfo>().cellsUnits.Contains(gameObject)) UnitReAssign();
            ResetDefaults();
        }
    }

    public void ResetDefaults()
    {
        engineSmoke.Stop();
        _transform.rotation = _defaultRotation;
        turret.transform.rotation = _defaultRotationForTurret;
        isAnimationIsAttack = false;
        if (targetPosition != _transform.parent.transform) SetPositionToSpawnPoint();
        else targetPosition = null;
        attackTarget = null;
    }
    public void UnitReAssign()
    {
        //TODO: добавить удаление битвы из списка битв при доезжании до точки чтобы можно было монитроить проверку в других функциях,
        //а то если юниты едут после победы в пустую клетку, ии может его укрепить
        _attackSector.cellsUnits.Add(gameObject);
        var previousSector = GetComponentInParent<SectorInfo>();;
        previousSector.cellsUnits.Remove(gameObject);
        previousSector.CalculateFreeCapacity();
        previousSector.lightTank--;
        _attackSector.lightTank++;
        if(previousSector.isPlayerOwner) _attackSector.isPlayerOwner = true;
        else _attackSector.isPlayerOwner = false;
        _attackSector.ChangeSectorColor();
        transform.parent = targetPosition;
        previousSector.CalculateFreeCapacity();
        if (currentBattle.AttackSide.Contains(gameObject))
        {
            currentBattle.AttackSide.Remove(gameObject);
            Battle.Current.CheckIsNeedToDestroyBattle(currentBattle);
        }
        AiInfo.Current.GetAISectors();
    }

    public void UnitReAssignForAllySector()
    {
        if (!_attackSector.cellsUnits.Contains(gameObject)) _attackSector.cellsUnits.Add(gameObject);
        var previousSector = GetComponentInParent<SectorInfo>();
        previousSector.cellsUnits.Remove(gameObject);
        previousSector.CalculateFreeCapacity();
        previousSector.lightTank--;
        _attackSector.lightTank++;
        transform.parent = _attackSector.spawnPoints[_cellPosition].transform;
        GetComponentInParent<SectorInfo>().CalculateFreeCapacity();
    }

    private void Start()
    {
        _transform = transform;
        _rigidbody = GetComponent<Rigidbody>();
        _defaultRotation = _transform.rotation;
        _defaultRotationForTurret = turret.transform.rotation;
        etalonHp = hp;
    }
    private void Update()
    {
        if(!CanvasesManager.IsGameStarted) return;
        if (targetPosition != null)
        {
            if (attackTarget)
            {
                turret.transform.LookAt(attackTarget.transform);
                if ((_transform.position - attackTarget.transform.position).magnitude > attackRange)
                {
                    MovingToCell();
                }
            }
            else MovingToCell();
        }

        if (hp <= 0) TrooperIsDead();
    }

    private void FixedUpdate()
    {
        if(!CanvasesManager.IsGameStarted) return;
        HpRegeneration();
    }

    public void SetCurrentBattle(Battle.BattleInSector battleInSector)
    {
        currentBattle = battleInSector;
    }

    public void SetAttackSector(SectorInfo sectorInfo)
    {
        _attackSector = sectorInfo;
    }

    public void SetAttackSectorToParentSector()
    {
        _attackSector = GetComponentInParent<SectorInfo>();
    }

    public void SetPositionToSpawnPoint()
    {
        targetPosition = transform.parent.transform;
    }
    public float ReturnThreat()
    {
        if(GetComponentInParent<SectorInfo>().isPlayerOwner) return 4f * (1 + 0.1f * ResearchScript.Current.lightTankUpgradeLvl);
        return 4f * (1 + 10f / ResearchScript.Current.ailightTankUpgradeLvl);
    }

    public int ReturnTypeOfUnit()
    {
        return 4;
    }

    public Transform ReturnTargetPosition()
    {
        return targetPosition;
    }

    public float ReturnPercentageHpOfUnit()
    {
        return hp / etalonHp;
    }

    public void HpRegeneration()
    {
        if(Battle.Current.CheckIsSectorInAnyBattle(GetComponentInParent<SectorInfo>())) return;
        if (hp < etalonHp)
        {
            hp += hp * Settings.Current.hpRegenerationPercent;
            if (hp > etalonHp) hp = etalonHp;
        }
    }
}

